@extends('users.layouts.app')

{{--@section('title', 'Dashboard')--}}

@section('content')

<div class="offcanvas-overlay"></div>

<!-- Hero/Intro Slider Start -->
<div class="section ">
    <div class="hero-slider swiper-container slider-nav-style-1 slider-dot-style-1 dot-color-white">
        <!-- Hero slider Active -->
        <div class="swiper-wrapper">
            <!-- Single slider item -->
            <div class="hero-slide-item slider-height-2 swiper-slide d-flex">
                <div class="hero-bg-image">
                    <img src="{{asset('assets/images/slider-image/slider-2-1.jpg')}}" alt="">
                </div>
            </div>
            <!-- Single slider item -->
            <div class="hero-slide-item slider-height-2 swiper-slide d-flex text-center">
                <div class="hero-bg-image">
                    <img src="{{asset('assets/images/slider-image/slider-2-2.jpg')}}" alt="">
                </div>
            </div>
        </div>
        <!-- Add Pagination -->
        <div class="swiper-pagination swiper-pagination-white"></div>
        <!-- Add Arrows -->
        <div class="swiper-buttons">
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>
</div>

<!-- Hero/Intro Slider End -->

<!-- Banner Section Start -->
<div class="section pb-100px pt-100px">
    <div class="container">
        <!-- Banners Start -->
        <div class="row">
            <!-- Banner Start -->
            <div class="col-lg-4 col-12 mb-md-30px mb-lm-30px" data-aos="fade-up" data-aos-delay="200">
                <div class="banner-2">
                    <img src="assets/images/banner/5.jpg" alt="" />
                    <div class="info justify-content-start">
                        <div class="content align-self-center">
                            <h3 class="title">
                                New Office Chair <br /> Collection
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Banner End -->

            <!-- Banner Start -->
            <div class="col-lg-4 col-12 mb-md-30px mb-lm-30px" data-aos="fade-up" data-aos-delay="400">
                <div class="banner-2">
                    <img src="assets/images/banner/6.jpg" alt="" />
                    <div class="info justify-content-start">
                        <div class="content align-self-center">
                            <h3 class="title">
                                Living Room <br /> Collection </h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Banner End -->

            <!-- Banner Start -->
            <div class="col-lg-4 col-12" data-aos="fade-up" data-aos-delay="600">
                <div class="banner-2">
                    <img src="assets/images/banner/7.jpg" alt="" />
                    <div class="info justify-content-start">
                        <div class="content align-self-center">
                            <h3 class="title">
                                Children Room <br /> Collection
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Banner End -->
        </div>
        <!-- Banners End -->
    </div>
</div>
<!-- Banner Section End -->

<!-- Product tab Area Start -->
<div class="section product-tab-area">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center" data-aos="fade-up">
                <div class="section-title mb-0">
                    <h2 class="title">Sản Phẩm Của Chúng Tôi</h2>
                    <p class="sub-title mb-30px">Hy-gge luôn mang đến những sản phẩm chất lượng và dịch vụ tốt nhất tới khách hàng</p>
                </div>
            </div>

            <!-- Tab Start -->
            <div class="col-md-12 text-center mb-40px" data-aos="fade-up" data-aos-delay="200">
                <ul class="product-tab-nav nav justify-content-center">
                    <li class="nav-item"><a class="nav-link active" data-bs-toggle="tab" href="#tab-product-new-arrivals">Sản Phẩm Giảm Giá</a></li>
{{--                    <li class="nav-item"><a class="nav-link" data-bs-toggle="tab" href="#tab-product-best-sellers">Sản Phẩm Giảm Giá</a></li>--}}
                </ul>
            </div>
            <!-- Tab End -->
        </div>
        <div class="row">
            <div class="col">
                <div class="tab-content">
                    <!-- 1st tab start -->
{{--                    <div class="tab-pane fade show active" id="tab-product-new-arrivals">--}}
{{--                        <div class="row">--}}
{{--                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-30px" data-aos="fade-up" data-aos-delay="200">--}}
{{--                                <!-- Single Prodect -->--}}
{{--                                <div class="product">--}}
{{--                                    <div class="thumb">--}}
{{--                                        <a href="shop-left-sidebar.html" class="image">--}}
{{--                                            <img src="assets/images/product-image/1.jpg" alt="Product" />--}}
{{--                                            <img class="hover-image" src="assets/images/product-image/2.jpg" alt="Product" />--}}
{{--                                        </a>--}}
{{--                                        <span class="badges">--}}
{{--                                                <span class="new">New</span>--}}
{{--                                            </span>--}}
{{--                                        <div class="actions">--}}
{{--                                            <a href="wishlist.html" class="action wishlist" title="Wishlist"><i--}}
{{--                                                    class="icon-heart"></i></a>--}}
{{--                                            <a href="#" class="action quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#exampleModal"><i--}}
{{--                                                    class="icon-size-fullscreen"></i></a>--}}
{{--                                            <a href="compare.html" class="action compare" title="Compare"><i--}}
{{--                                                    class="icon-refresh"></i></a>--}}
{{--                                        </div>--}}
{{--                                        <button title="Add To Cart" class=" add-to-cart">Thêm Vào Giỏ</button>--}}
{{--                                    </div>--}}
{{--                                    <div class="content">--}}
{{--                                        <h5 class="title"><a href="shop-left-sidebar.html">Simple minimal Chair </a></h5>--}}
{{--                                        <span class="price">--}}
{{--                                                <span class="new">$38.50</span>--}}
{{--                                            </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-30px" data-aos="fade-up" data-aos-delay="400">--}}
{{--                                <!-- Single Prodect -->--}}
{{--                                <div class="product">--}}
{{--                                    <div class="thumb">--}}
{{--                                        <a href="shop-left-sidebar.html" class="image">--}}
{{--                                            <img src="assets/images/product-image/3.jpg" alt="Product" />--}}
{{--                                            <img class="hover-image" src="assets/images/product-image/4.jpg" alt="Product" />--}}
{{--                                        </a>--}}
{{--                                        <span class="badges">--}}
{{--                                                <span class="sale">-10%</span>--}}
{{--                                            <span class="new">New</span>--}}
{{--                                            </span>--}}
{{--                                        <div class="actions">--}}
{{--                                            <a href="wishlist.html" class="action wishlist" title="Wishlist"><i--}}
{{--                                                    class="icon-heart"></i></a>--}}
{{--                                            <a href="#" class="action quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#exampleModal"><i--}}
{{--                                                    class="icon-size-fullscreen"></i></a>--}}
{{--                                            <a href="compare.html" class="action compare" title="Compare"><i--}}
{{--                                                    class="icon-refresh"></i></a>--}}
{{--                                        </div>--}}
{{--                                        <button title="Add To Cart" class=" add-to-cart">Add--}}
{{--                                            To Cart</button>--}}
{{--                                    </div>--}}
{{--                                    <div class="content">--}}
{{--                                        <h5 class="title"><a href="shop-left-sidebar.html">Wooden decorations</a></h5>--}}
{{--                                        <span class="price">--}}
{{--                                                <span class="new">$38.50</span>--}}
{{--                                            <span class="old">$48.50</span>--}}
{{--                                            </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!-- Single Prodect -->--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-30px" data-aos="fade-up" data-aos-delay="600">--}}
{{--                                <!-- Single Prodect -->--}}
{{--                                <div class="product">--}}
{{--                                    <div class="thumb">--}}
{{--                                        <a href="shop-left-sidebar.html" class="image">--}}
{{--                                            <img src="assets/images/product-image/5.jpg" alt="Product" />--}}
{{--                                            <img class="hover-image" src="assets/images/product-image/6.jpg" alt="Product" />--}}
{{--                                        </a>--}}
{{--                                        <span class="badges">--}}
{{--                                                <span class="sale">-7%</span>--}}
{{--                                            </span>--}}
{{--                                        <div class="actions">--}}
{{--                                            <a href="wishlist.html" class="action wishlist" title="Wishlist"><i--}}
{{--                                                    class="icon-heart"></i></a>--}}
{{--                                            <a href="#" class="action quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#exampleModal"><i--}}
{{--                                                    class="icon-size-fullscreen"></i></a>--}}
{{--                                            <a href="compare.html" class="action compare" title="Compare"><i--}}
{{--                                                    class="icon-refresh"></i></a>--}}
{{--                                        </div>--}}
{{--                                        <button title="Add To Cart" class=" add-to-cart">Add--}}
{{--                                            To Cart</button>--}}
{{--                                    </div>--}}
{{--                                    <div class="content">--}}
{{--                                        <h5 class="title"><a href="shop-left-sidebar.html">High quality vase bottle</a></h5>--}}
{{--                                        <span class="price">--}}
{{--                                                <span class="new">$30.50</span>--}}
{{--                                            <span class="old">$38.00</span>--}}
{{--                                            </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-30px" data-aos="fade-up" data-aos-delay="800">--}}
{{--                                <!-- Single Prodect -->--}}
{{--                                <div class="product">--}}
{{--                                    <div class="thumb">--}}
{{--                                        <a href="shop-left-sidebar.html" class="image">--}}
{{--                                            <img src="assets/images/product-image/7.jpg" alt="Product" />--}}
{{--                                            <img class="hover-image" src="assets/images/product-image/8.jpg" alt="Product" />--}}
{{--                                        </a>--}}
{{--                                        <span class="badges">--}}
{{--                                                <span class="new">New</span>--}}
{{--                                            </span>--}}
{{--                                        <div class="actions">--}}
{{--                                            <a href="wishlist.html" class="action wishlist" title="Wishlist"><i--}}
{{--                                                    class="icon-heart"></i></a>--}}
{{--                                            <a href="#" class="action quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#exampleModal"><i--}}
{{--                                                    class="icon-size-fullscreen"></i></a>--}}
{{--                                            <a href="compare.html" class="action compare" title="Compare"><i--}}
{{--                                                    class="icon-refresh"></i></a>--}}
{{--                                        </div>--}}
{{--                                        <button title="Add To Cart" class=" add-to-cart">Add--}}
{{--                                            To Cart</button>--}}
{{--                                    </div>--}}
{{--                                    <div class="content">--}}
{{--                                        <h5 class="title"><a href="shop-left-sidebar.html">Living & Bedroom Chair</a></h5>--}}
{{--                                        <span class="price">--}}
{{--                                                <span class="new">$38.50</span>--}}
{{--                                            </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!-- Single Prodect -->--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <div class="tab-pane fade show active" id="tab-product-new-arrivals">
                        <div class="row">

                            @foreach($saleProducts as $sale)

                                <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-30px" data-aos="fade-up" data-aos-delay="200">
                                    <!-- Single Prodect -->
                                    <form action="{{route('cart.addCart')}}"  method="POST">
                                        @csrf
                                        <input hidden name="product_id" value="{{$sale->id}}">
                                        <div class="product">
                                            <div class="thumb">
                                                <a href="{{route('user.product.show', $sale->id)}}" class="image">
                                                    <img src="{{ asset('uploads/products/'.$sale->image) }}" alt="Product" width="276px" height="324.7px"/>
                                                    <img class="hover-image" src="{{ asset('uploads/products/'.$sale->image) }}" alt="Product" width="276px" height="324.7px"/>
                                                </a>
                                                <span class="badges">
                                            <span class="sale">-{{$sale->ticket}}%</span>
                                        </span>
                                                <div class="actions">
                                                    <a class="action wishlist" title="Wishlist"><i
                                                            class="icon-heart"></i></a>
                                                    <a class="action quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#exampleModal"><i
                                                            class="icon-size-fullscreen"></i></a>

                                                </div>
                                                <button type="submit" title="Add To Cart" class=" add-to-cart">Thêm Vào Giỏ</button>
                                            </div>
                                            <div class="content">
                                                <h5 class="title"><a href="{{route('user.product.show', $sale->id)}}">{{$sale->name}} </a></h5>
                                                <span class="price">
                                                <span class="new">{{$sale->price * $sale->ticket / 100}} đ</span>
                                                <span class="old">{{$sale->price}} đ</span>
                                            </span>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            @endforeach

                        </div>
                    </div>
                    <!-- 1st tab end -->
                    <!-- 2nd tab start -->
{{--                    <div class="tab-pane fade" id="tab-product-best-sellers">--}}
{{--                        <div class="row">--}}

{{--                            @foreach($saleProducts as $sale)--}}

{{--                            <div class="col-lg-3 col-md-6 col-sm-6 col-xs-6 mb-30px" data-aos="fade-up" data-aos-delay="200">--}}
{{--                                <!-- Single Prodect -->--}}
{{--                                <form action="{{route('cart.addCart')}}"  method="POST">--}}
{{--                                    @csrf--}}
{{--                                <input hidden name="product_id" value="{{$sale->id}}">--}}
{{--                                <div class="product">--}}
{{--                                    <div class="thumb">--}}
{{--                                        <a href="{{route('user.product.show', $sale->id)}}" class="image">--}}
{{--                                            <img src="{{ asset('uploads/products/'.$sale->image) }}" alt="Product" width="276px" height="324.7px"/>--}}
{{--                                            <img class="hover-image" src="{{ asset('uploads/products/'.$sale->image) }}" alt="Product" width="276px" height="324.7px"/>--}}
{{--                                        </a>--}}
{{--                                        <span class="badges">--}}
{{--                                            <span class="sale">-{{$sale->ticket}}%</span>--}}
{{--                                        </span>--}}
{{--                                        <div class="actions">--}}
{{--                                            <a class="action wishlist" title="Wishlist"><i--}}
{{--                                                    class="icon-heart"></i></a>--}}
{{--                                            <a class="action quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#exampleModal"><i--}}
{{--                                                    class="icon-size-fullscreen"></i></a>--}}

{{--                                        </div>--}}
{{--                                        <button type="submit" title="Add To Cart" class=" add-to-cart">Thêm Vào Giỏ</button>--}}
{{--                                    </div>--}}
{{--                                    <div class="content">--}}
{{--                                        <h5 class="title"><a href="{{route('user.product.show', $sale->id)}}">{{$sale->name}} </a></h5>--}}
{{--                                        <span class="price">--}}
{{--                                                <span class="new">{{$sale->price * $sale->ticket / 100}} đ</span>--}}
{{--                                                <span class="old">{{$sale->price}} đ</span>--}}
{{--                                            </span>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                </form>--}}
{{--                            </div>--}}
{{--                            @endforeach--}}

{{--                        </div>--}}
{{--                    </div>--}}
                    <!-- 2nd tab end -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Product tab Area End -->

<!-- Banner Section Start -->
<div class="section pb-100px pt-100px">
    <div class="container-fluid">
        <!-- Banners Start -->
        <div class="row">
            <!-- Banner Start -->
            <div class="col-lg-6 col-md-6 col-12 mb-lm-30px" data-aos="fade-up" data-aos-delay="200">
                <a class="banner-3">
                    <img src="{{asset('assets/images/banner/1.jpg')}}" alt="" />
                    <div class="info justify-content-end">
                        <div class="content align-self-center">
                            <h3 class="title">
                                Nội thất giảm giá <br /> cho mùa hè
                            </h3>
                            <p>Siêu giảm giá</p>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Banner End -->

            <!-- Banner Start -->
            <div class="col-lg-6 col-md-6 col-12" data-aos="fade-up" data-aos-delay="200">
                <a class="banner-3">
                    <img src="{{asset('assets/images/banner/2.jpg')}}" alt="" />
                    <div class="info justify-content-start">
                        <div class="content align-self-center">
                            <h3 class="title">
                                Ghế văn phòng <br /> cho ưu đãi tốt</h3>
                            <p>Siêu giảm giá</p>
                        </div>
                    </div>
                </a>
            </div>
            <!-- Banner End -->
        </div>
        <!-- Banners End -->
    </div>
</div>
<!-- Banner Section End -->

<!-- New Product Start -->
<div class="section pb-100px">
    <div class="container">
        <!-- section title start -->
        <div class="row">
            <div class="col-md-12" data-aos="fade-up">
                <div class="section-title text-center mb-11">
                    <h2 class="title">Sản Phẩm Mới</h2>
                    <p class="sub-title">Hy-gge store luôn cập nhật su hướng & mẫu mã đa dạng phục vụ quý khách
                    </p>
                </div>
            </div>
        </div>
        <!-- section title start -->
        <div class="new-product-slider swiper-container slider-nav-style-1" data-aos="fade-up" data-aos-delay="200">
            <div class="new-product-wrapper swiper-wrapper">
                @foreach($newProducts as $newProduct)
                <!-- Single Prodect -->
                <div class="new-product-item swiper-slide">
                    <form action="{{route('cart.addCart')}}"  method="POST">
                        @csrf
                        <input hidden name="product_id" value="{{$newProduct->id}}">
                    <div class="product">
                        <div class="thumb">
                            <a href="{{route('user.product.show', $newProduct->id)}}" class="image">
                                <img src="{{ asset('uploads/products/'.$newProduct->image) }}" alt="Product" width="276px" height="324.7px" />
                                <img class="hover-image" src="{{ asset('uploads/products/'.$newProduct->image) }}" alt="Product" width="276px" height="324.7px" />
                            </a>
                            <span class="badges">
                                    <span class="new">New</span>
                                <span class="sale">-{{$newProduct->ticket}}%</span>
                                </span>
                            <div class="actions">
                                <a class="action wishlist" title="Wishlist"><i
                                        class="icon-heart"></i></a>
                                <a class="action quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#exampleModal"><i
                                        class="icon-size-fullscreen"></i></a>
                            </div>
                            <button title="Add To Cart" class=" add-to-cart">Thêm Vào Giỏ</button>
                        </div>
                        <div class="content">
                            <h5 class="title"><a href="{{route('user.product.show', $newProduct->id)}}">{{$newProduct->name}}</a></h5>
                            @if($newProduct->ticket > 0)
                            <span class="price">
                                    <span class="new">{{$newProduct->price * $newProduct->ticket / 100}} đ</span>
                                                <span class="old">{{$newProduct->price}} đ</span>
                                </span>
                            @else
                                <span class="price">
                                    <span class="new">{{$newProduct->price}} đ</span>
                                </span>
                            @endif
                        </div>
                    </div>
                    </form>
                </div>
                <!-- Single Prodect -->
                @endforeach
            </div>
            <!-- Add Arrows -->
            <div class="swiper-buttons">
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
</div>

<!-- New Product End -->


<!--  Blog area Start -->
<div class="main-blog-area pb-100px">
    <div class="container">
        <!-- section title start -->
        <div class="row">
            <div class="col-md-12" data-aos="fade-up">
                <div class="section-title text-center mb-11">
                    <h2 class="title">Bài Viết Mới</h2>
                    <p class="sub-title">Hy-gge store luôn cập nhật xu hướng định hướng thay đổi diện mạo không gian phù hợp với khách hàng
                    </p>
                </div>
            </div>
        </div>
        <!-- section title start -->
        <div class="blog-slider swiper-container slider-nav-style-1" data-aos="fade-up" data-aos-delay="200">
            <!-- Start single blog -->
            <div class="swiper-wrapper">
                @foreach($newPosts as $newPost)
                <div class="single-blog swiper-slide">
                    <div class="blog-image">
                        <a href="blog-single-left-sidebar.html"><img src="{{ asset('uploads/products/'.$newPost->image) }}" class="img-responsive w-100" alt="" width="372px" height="403.09px"></a>
                    </div>
                    <div class="blog-text">
                        <div class="blog-athor-date">
                            <a class="blog-date" >{{ $newPost->created_at->format('d M')}}</a>
                        </div>
                        <h5 class="blog-heading"><a class="blog-heading-link" href="blog-single-left-sidebar.html">{{$newPost->title}}</a></h5>
                        <p class="blog-detail-text" style=" height:104px">{{substr($newPost->description, 0, 200)}}</p>

                        <a href="#" class="btn btn-lg btn-hover-color-primary btn-color-dark mt-25px">Đọc Thêm</a>
                    </div>
                </div>
            @endforeach
                <!-- End single blog -->
            </div>
            <!-- Add Arrows -->
            <div class="swiper-buttons">
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>
    </div>
</div>
<!--  Blog area End -->
@endsection
