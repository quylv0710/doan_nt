<head>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge" />
    <title>HY-GGE</title>
    <meta name="description" content="240+ Best Bootstrap Templates are available on this website. Find your template for your project compatible with the most popular HTML library in the world." />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="canonical" href="https://htmldemo.hasthemes.com/furns/" />

    <!-- Open Graph (OG) meta tags are snippets of code that control how URLs are displayed when shared on social media  -->
    <meta property="og:locale" content="en_US" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="Furns - Responsive eCommerce HTML Template" />
    <meta property="og:url" content="PAGE_URL" />
    <meta property="og:site_name" content="Furns - Responsive eCommerce HTML Template" />
    <!-- For the og:image content, replace the # with a link of an image -->
    <meta property="og:image" content="#" />
    <meta property="og:description" content="Furns - Responsive eCommerce HTML Template" />
    <meta name="robots" content="noindex, follow" />
    <!-- Add site Favicon -->
    <link rel="icon" href="{{asset('assets/images/favicon/favicon.png')}}" sizes="32x32" />
    <link rel="icon" href="{{asset('assets/images/favicon/favicon.png')}}" sizes="192x192" />
    <link rel="apple-touch-icon" href="{{asset('assets/images/favicon/favicon.png')}}" />
    <meta name="msapplication-TileImage" content="{{asset('assets/images/favicon/favicon.png')}}" />
    <!-- Structured Data  -->
    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "WebSite",
            "name": "Replace_with_your_site_title",
            "url": "Replace_with_your_site_URL"
        }
    </script>

    <!-- vendor css (Bootstrap & Icon Font) -->
    <!-- <link rel="stylesheet" href="assets/css/vendor/bootstrap.min.css" />
    <!-- <link rel="stylesheet" href="assets/css/vendor/simple-line-icons.css" />
    <link rel="stylesheet" href="assets/css/vendor/ionicons.min.css" /> -->

    <!-- plugins css (All Plugins Files) -->
    <!-- <link rel="stylesheet" href="assets/css/plugins/animate.css" />
    <link rel="stylesheet" href="assets/css/plugins/swiper-bundle.min.css" />
    <link rel="stylesheet" href="assets/css/plugins/jquery-ui.min.css" />
    <link rel="stylesheet" href="assets/css/plugins/jquery.lineProgressbar.css">
    <link rel="stylesheet" href="assets/css/plugins/nice-select.css" />
    <link rel="stylesheet" href="assets/css/plugins/venobox.css" /> -->

    <!-- Use the minified version files listed below for better performance and remove the files listed above -->
    <link rel="stylesheet" href="{{asset('assets/css/vendor/vendor.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/plugins/plugins.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/css/style.min.css')}}">

    <!-- Main Style -->
    <!-- <link rel="stylesheet" href="assets/css/style.css" /> -->
        <style>
            *{
                margin: 0;
                padding: 0;
            }
            .stars {
                float: left;
                height: 46px;
                padding: 0 10px;
            }
            .stars:not(:checked) > input {
                position:absolute;
                top:-9999px;
            }
            .stars:not(:checked) > label {
                float:right;
                width:1em;
                overflow:hidden;
                white-space:nowrap;
                cursor:pointer;
                font-size:30px;
                color:#ccc;
            }
            .stars:not(:checked) > label:before {
                content: '★ ';
            }
            .stars > input:checked ~ label {
                color: #ffc700;
            }
            .stars:not(:checked) > label:hover,
            .stars:not(:checked) > label:hover ~ label {
                color: #deb217;
            }
            .stars > input:checked + label:hover,
            .stars > input:checked + label:hover ~ label,
            .stars > input:checked ~ label:hover,
            .stars > input:checked ~ label:hover ~ label,
            .stars > label:hover ~ input:checked ~ label {
                color: #c59b08;
            }

            .stars2 {
                float: left;
                height: 46px;
                padding: 0 10px;
            }
            .stars2:not(:checked) > input {
                position:absolute;
                top:-9999px;
            }
            .stars2:not(:checked) > label {
                float:right;
                width:1em;
                overflow:hidden;
                white-space:nowrap;
                cursor:pointer;
                font-size:30px;
                color:#ccc;
            }
            .stars2:not(:checked) > label:before {
                content: '★ ';
            }
            .stars2 > input:checked ~ label {
                color: #ffc700;
            }
        </style>
</head>
