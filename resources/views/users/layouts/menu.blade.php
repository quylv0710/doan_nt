<!-- Header Area start  -->
<div class="header section">
    <!-- Header Top Message Start -->
    <div class="header-top section-fluid bg-black"></div>
    <!-- Header Top  End -->
    <!-- Header Bottom  Start -->
    <div class="header-bottom d-none d-lg-block">
        <div class="container position-relative">
            <div class="row align-self-center">
                <!-- Header Logo Start -->
                <div class="col-auto align-self-center">
                    <div class="header-logo">
                        <a href="{{route('home.index')}}"><img src="{{asset('assets/images/logo/hygge2.jpg')}}" alt="Site Logo" /></a>
                    </div>
                </div>
                <!-- Header Logo End -->

                <!-- Header Action Start -->
                <div class="col align-self-center">
                    <div class="header-actions">
                        <div class="header_account_list">
                            <a href="javascript:void(0)" class="header-action-btn search-btn"><i
                                    class="icon-magnifier"></i></a>
                            <div class="dropdown_search">
                                <form class="action-form" action="{{route('user.product.list')}}" method="GET">
                                    <input name="name" value="{{request('name')}}" class="form-control" placeholder="Nhập tên sản phẩm cần tìm..." type="text">
                                    <button class="submit" type="submit"><i class="icon-magnifier"></i></button>
                                </form>
                            </div>
                        </div>
                        <!-- Single Wedge Start -->
                        <div class="header-bottom-set dropdown">
                            <button class="dropdown-toggle header-action-btn" data-bs-toggle="dropdown"><i
                                    class="icon-user"></i></button>
                            <ul class="dropdown-menu dropdown-menu-right">
                                @guest
                                    @if (Route::has('login'))
                                        <li><a class="dropdown-item"href="{{ route('login') }}">{{ __('Login') }}</a></li>
                                    @endif

{{--                                    @if (Route::has('register'))--}}
{{--                                        <li class="nav-item">--}}
{{--                                            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>--}}
{{--                                        </li>--}}
{{--                                    @endif--}}
                                @else
                                    <li><a class="dropdown-item">{{ Auth::user()->name }}</a></li>
                                    <li><a class="dropdown-item" href="{{ route('logout') }}"
                                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                @endguest
                            </ul>
                        </div>
                        <!-- Single Wedge End -->
                        <a href="{{route('cart.index')}}" class="header-action-btn">
                            <i class="icon-handbag"></i>
                            <span class="header-action-num">{{$countProductInCart}}</span>
                        </a>
                        <a href="#offcanvas-mobile-menu" class="header-action-btn header-action-btn-menu offcanvas-toggle d-lg-none">
                            <i class="icon-menu"></i>
                        </a>
                    </div>
                </div>
                <!-- Header Action End -->
            </div>
        </div>
    </div>
    <!-- Header Bottom  End -->
    <!-- Main Menu Start -->
    <div class="bg-gray d-none d-lg-block sticky-nav">
        <div class="container position-relative">
            <div class="row">
                <div class="col-md-12 align-self-center">
                    <div class="main-menu manu-color-white">
                        <ul>
                            <li class="dropdown"><a href="{{route('home.index')}}">Trang chủ</a>
                            </li>
                            <li><a href="{{route('user.about')}}">Giới thiệu</a></li>
                            <li class="dropdown"><a href="#">Danh mục sản phẩm <i class="ion-ios-arrow-down"></i></a>
                                <ul class="sub-menu">
                                    @foreach($categories as $category)
                                    <li><a href="{{route('user.product.listCategory',$category->id)}}">{{$category->name}}</a></li>
                                    @endforeach
                                </ul>
                            </li>
                            <li class="dropdown "><a href="{{route('user.posts.index')}}">Tin tức </a></li>
                            <li><a href="{{route('user.contact')}}">Liên hệ</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main Menu End -->
</div>
<!-- Header Area End  -->
