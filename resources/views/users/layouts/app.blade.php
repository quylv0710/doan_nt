<!DOCTYPE html>
<html lang="zxx">

@include('users.layouts.header')

<body>

@include('users.layouts.menu')

@yield('content')

@include('users.layouts.footer')
</body>

</html>
