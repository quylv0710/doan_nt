@extends('users.layouts.app')

@section('content')
    <div class="offcanvas-overlay"></div>
    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area">
        <h5 class="breadcrumb-title" style="text-align: center;">Liên hệ</h5>
    </div>

    <!-- breadcrumb-area end -->

    <!-- contact area start -->
    <div class="contact-area pb-100px pt-100px">
        <div class="container">
            <div class="contact-map mb-50px">
                <div id="mapid" data-aos="fade-up" data-aos-delay="200">
                    <div class="mapouter">
                        <div class="gmap_canvas">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3109.003969637176!2d105.82857773917215!3d21.03102899848425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135abbcff60c709%3A0xc758193f8864687b!2sHYGGE%20-%20sieu%20thi%20Noi%20that!5e0!3m2!1svi!2s!4v1666993111727!5m2!1svi!2s" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                            <a href="https://sites.google.com/view/maps-api-v2/mapv2"></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- contact area end -->
@endsection
