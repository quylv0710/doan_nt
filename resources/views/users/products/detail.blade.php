@extends('users.layouts.app')

@section('content')
    <!-- OffCanvas Cart Start -->
    <div id="offcanvas-cart" class="offcanvas offcanvas-cart">
        <div class="inner">
            <div class="head">
                <span class="title">Cart</span>
                <button class="offcanvas-close">×</button>
            </div>
            <div class="body customScroll">
                <ul class="minicart-product-list">
                    <li>
                        <a href="single-product.html" class="image"><img src="assets/images/product-image/1.jpg" alt="Cart product Image"></a>
                        <div class="content">
                            <a href="single-product.html" class="title">Walnut Cutting Board</a>
                            <span class="quantity-price">1 x <span class="amount">$91.86</span></span>
                            <a href="#" class="remove">×</a>
                        </div>
                    </li>
                    <li>
                        <a href="single-product.html" class="image"><img src="assets/images/product-image/2.jpg" alt="Cart product Image"></a>
                        <div class="content">
                            <a href="single-product.html" class="title">Lucky Wooden Elephant</a>
                            <span class="quantity-price">1 x <span class="amount">$453.28</span></span>
                            <a href="#" class="remove">×</a>
                        </div>
                    </li>
                    <li>
                        <a href="single-product.html" class="image"><img src="assets/images/product-image/3.jpg" alt="Cart product Image"></a>
                        <div class="content">
                            <a href="single-product.html" class="title">Fish Cut Out Set</a>
                            <span class="quantity-price">1 x <span class="amount">$87.34</span></span>
                            <a href="#" class="remove">×</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="foot">
                <div class="sub-total">
                    <table class="table">
                        <tbody>
                        <tr>
                            <td class="text-start">Sub-Total :</td>
                            <td class="text-end">$523.30</td>
                        </tr>
                        <tr>
                            <td class="text-start">Eco Tax (-2.00) :</td>
                            <td class="text-end">$4.52</td>
                        </tr>
                        <tr>
                            <td class="text-start">VAT (20%) :</td>
                            <td class="text-end">$104.66</td>
                        </tr>
                        <tr>
                            <td class="text-start">Total :</td>
                            <td class="text-end theme-color">$632.48</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <a href="cart.html" class="btn btn-dark btn-hover-primary mb-30px">view cart</a>
                    <a href="checkout.html" class="btn btn-outline-dark current-btn">checkout</a>
                </div>
                <p class="minicart-message">Free Shipping on All Orders Over $100!</p>
            </div>
        </div>
    </div>
    <!-- OffCanvas Cart End -->

    <!-- OffCanvas Menu Start -->
    <div id="offcanvas-mobile-menu" class="offcanvas offcanvas-mobile-menu">
        <button class="offcanvas-close"></button>
        <div class="inner customScroll">

            <div class="offcanvas-menu mb-20px">
                <ul>
                    <li><a href="#"><span class="menu-text">Home</span></a>
                        <ul class="sub-menu">
                            <li><a href="index.html"><span class="menu-text">Home 1</span></a></li>
                            <li><a href="index-2.html"><span class="menu-text">Home 2</span></a></li>
                        </ul>
                    </li>
                    <li><a href="about.html">About Us</a></li>
                    <li><a href="#"><span class="menu-text">Shop</span></a>
                        <ul class="sub-menu">
                            <li>
                                <a href="#"><span class="menu-text">Shop Page</span></a>
                                <ul class="sub-menu">
                                    <li><a href="shop-3-column.html">Shop 3 Column</a></li>
                                    <li><a href="shop-4-column.html">Shop 4 Column</a></li>
                                    <li><a href="shop-left-sidebar.html">Shop Grid Left Sidebar</a></li>
                                    <li><a href="shop-right-sidebar.html">Shop Grid Right Sidebar</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span class="menu-text">product Details Page</span></a>
                                <ul class="sub-menu">
                                    <li><a href="single-product.html">Product Single</a></li>
                                    <li><a href="single-product-variable.html">Product Variable</a></li>
                                    <li><a href="single-product-affiliate.html">Product Affiliate</a></li>
                                    <li><a href="single-product-group.html">Product Group</a></li>
                                    <li><a href="single-product-tabstyle-2.html">Product Tab 2</a></li>
                                    <li><a href="single-product-tabstyle-3.html">Product Tab 3</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span class="menu-text">Single Product Page</span></a>
                                <ul class="sub-menu">
                                    <li><a href="single-product-slider.html">Product Slider</a></li>
                                    <li><a href="single-product-gallery-left.html">Product Gallery Left</a>
                                    </li>
                                    <li><a href="single-product-gallery-right.html">Product Gallery Right</a>
                                    </li>
                                    <li><a href="single-product-sticky-left.html">Product Sticky Left</a></li>
                                    <li><a href="single-product-sticky-right.html">Product Sticky Right</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><span class="menu-text">Other Pages</span></a>
                                <ul class="sub-menu">
                                    <li><a href="cart.html">Cart Page</a></li>
                                    <li><a href="checkout.html">Checkout Page</a></li>
                                    <li><a href="compare.html">Compare Page</a></li>
                                    <li><a href="wishlist.html">Wishlist Page</a></li>
                                    <li><a href="my-account.html">Account Page</a></li>
                                    <li><a href="login.html">Login & Register Page</a></li>
                                    <li><a href="empty-cart.html">Empty Cart Page</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="#"><span class="menu-text">Pages</span></a>
                        <ul class="sub-menu">
                            <li><a href="404.html">404 Page</a></li>
                            <li><a href="privacy-policy.html">Privacy Policy</a></li>
                            <li><a href="faq.html">Faq Page</a></li>
                            <li><a href="coming-soon.html">Coming Soon Page</a></li>
                        </ul>
                    </li>
                    <li><a href="#"><span class="menu-text">Blog</span></a>
                        <ul class="sub-menu">
                            <li><a href="#"><span class="menu-text">Blog Grid</span></a>
                                <ul class="sub-menu">
                                    <li><a href="blog-grid-left-sidebar.html">Blog Grid Left Sidebar</a></li>
                                    <li><a href="blog-grid-right-sidebar.html">Blog Grid Right Sidebar</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><span class="menu-text">Blog List</span></a>
                                <ul class="sub-menu">
                                    <li><a href="blog-list-left-sidebar.html">Blog List Left Sidebar</a></li>
                                    <li><a href="blog-list-right-sidebar.html">Blog List Right Sidebar</a></li>
                                </ul>
                            </li>
                            <li><a href="#"><span class="menu-text">Blog Single</span></a>
                                <ul class="sub-menu">
                                    <li><a href="blog-single-left-sidebar.html">Blog Single Left Sidebar</a></li>
                                    <li><a href="blog-single-right-sidebar.html">Blog Single Right Sidbar</a></li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li><a href="contact.html">Contact Us</a></li>
                </ul>
            </div>
            <!-- OffCanvas Menu End -->

            <!-- Language Currency start -->
            <div class="offcanvas-userpanel mt-8">
                <ul>
                    <!-- Language Start -->
                    <li class="offcanvas-userpanel__role">
                        <a href="#">English <i class="ion-ios-arrow-down"></i></a>
                        <ul class="user-sub-menu">
                            <li><a class="current" href="#">English</a></li>
                            <li><a href="#"> Italiano</a></li>
                            <li><a href="#"> Français</a></li>
                            <li><a href="#"> Filipino</a></li>
                        </ul>
                    </li>
                    <!-- Language End -->
                    <!-- Currency Start -->
                    <li class="offcanvas-userpanel__role">
                        <a href="#">USD $ <i class="ion-ios-arrow-down"></i></a>
                        <ul class="user-sub-menu">
                            <li><a class="current" href="#">USD $</a></li>
                            <li><a href="#">EUR €</a></li>
                            <li><a href="#">POUND £</a></li>
                            <li><a href="#">FRANC ₣</a></li>
                        </ul>
                    </li>
                    <!-- Currency End -->
                </ul>
            </div>
            <!-- Language Currency End -->
            <div class="offcanvas-social mt-auto">
                <ul>
                    <li>
                        <a href="#"><i class="ion-social-facebook"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="ion-social-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="ion-social-google"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="ion-social-youtube"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="ion-social-instagram"></i></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- OffCanvas Menu End -->


    <div class="offcanvas-overlay"></div>
    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area">
        <h5 class="breadcrumb-title" style="text-align: center;">Chi Tiết Sản Phẩm</h5>
    </div>

    <!-- breadcrumb-area end -->

    <!-- Product Details Area Start -->
    <div class="product-details-area pt-100px pb-100px">
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-12 col-xs-12 mb-lm-30px mb-md-30px mb-sm-30px">
                    <!-- Swiper -->
                    <div class="swiper-container zoom-top">
                        <div class="swiper-wrapper">

                            <div class="swiper-slide zoom-image-hover">
                                <img class="img-responsive m-auto" src="{{ asset('uploads/products/'.$product->image) }}" alt="" width="476px" height="476px">
                            </div>
                            <div class="swiper-slide zoom-image-hover">
                                <img class="img-responsive m-auto" src="{{ asset('uploads/products/'.$product->image) }}" alt="" width="476px" height="476px">
                            </div>
                            <div class="swiper-slide zoom-image-hover">
                                <img class="img-responsive m-auto" src="{{ asset('uploads/products/'.$product->image) }}" alt="" width="476px" height="476px">
                            </div>
                            <div class="swiper-slide zoom-image-hover">
                                <img class="img-responsive m-auto" src="{{ asset('uploads/products/'.$product->image) }}" alt="" width="476px" height="476px">
                            </div>
                        </div>
                    </div>
                    <div class="swiper-container zoom-thumbs slider-nav-style-1 small-nav mt-15px mb-15px">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <img class="img-responsive m-auto" src="{{ asset('uploads/products/'.$product->image) }}" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img class="img-responsive m-auto" src="{{ asset('uploads/products/'.$product->image) }}" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img class="img-responsive m-auto" src="{{ asset('uploads/products/'.$product->image) }}" alt="">
                            </div>
                            <div class="swiper-slide">
                                <img class="img-responsive m-auto" src="{{ asset('uploads/products/'.$product->image) }}" alt="">
                            </div>
                        </div>
                        <!-- Add Arrows -->
                        <div class="swiper-buttons">
                            <div class="swiper-button-next"></div>
                            <div class="swiper-button-prev"></div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-7 col-sm-12 col-xs-12" data-aos="fade-up" data-aos-delay="200">
                    <div class="product-details-content quickview-content">
                        <form action="{{route('cart.addCart.detail', $product->id)}}"  method="POST">
                            @csrf
                            <input hidden name="product_id" value="{{$product->id}}">
                        <h2>{{$product->name}}</h2>
                        <p class="reference">Mã SP:<span> {{$product->id}}</span></p>
                        <div class="pro-details-rating-wrap">
                            <div class="rating-product">
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                                <i class="ion-android-star"></i>
                            </div>
                        </div>
                        @if($product->ticket == 0)
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price not-cut" style="color: #ff7004">{{number_format($product->price)}} đ</li>
                                </ul>
                            </div>
                        @elseif($product->ticket > 0)
                            <div class="pricing-meta">
                                <ul>
                                    <li class="old-price not-cut" style="color: #ff7004">{{number_format($product->price * $product->ticket /100)}} đ</li>
                                    <li class="old-price not-cut" style="color:#999999; text-decoration: line-through;">
                                        <p style="color:#999999;">{{number_format($product->price)}} đ</p>
                                    </li>
                                </ul>
                            </div>
                        @endif

                        <p class="reference">Trạng thái:<span> Còn hàng</span></p>
                        <div class="pro-details-quality">
                            <div class="cart-plus-minus">
                                <input class="cart-plus-minus-box" type="text" id="quantity" name="quantity" min="1" max="50" value="1"/>
                            </div>
                            <div class="pro-details-cart">
                                <button class="add-cart btn btn-primary btn-hover-primary ml-4" href="#">  Thêm Vào Giỏ</button>
                            </div>
                        </div>
                        <div class="pro-details-wish-com">
                            <div class="pro-details-wishlist">
                                <a><i class="ion-android-favorite-outline"></i>Thêm vào danh sách yêu thích</a>
                            </div>
                            <div class="pro-details-compare">
                                <a><i class="ion-ios-shuffle-strong"></i>Thêm vào để so sánh</a>
                            </div>
                        </div>
                        <div class="pro-details-social-info">
                            <span>Share</span>
                            <div class="social-info pt-1">
                                <ul class="d-flex">
                                    <li>
                                        <a><i class="ion-social-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a><i class="ion-social-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a><i class="ion-social-google"></i></a>
                                    </li>
                                    <li>
                                        <a><i class="ion-social-instagram"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="pro-details-policy">
                            <ul>
                                <li><img src="{{asset('assets/images/icons/policy.png')}}" alt="" /><span>Chính Sách Bảo Mật</span></li>
                                <li><img src="{{asset('assets/images/icons/policy-2.png')}}" alt="" /><span>Chính Sách Giao Hàng</span></li>
                                <li><img src="{{asset('assets/images/icons/policy-3.png')}}" alt="" /><span>Chính Sách TRả Hàng</span></li>
                            </ul>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- product details description area start -->
    <div class="description-review-area pb-100px" data-aos="fade-up" data-aos-delay="200">
        <div class="container">
            <div class="description-review-wrapper">
                <div class="description-review-topbar nav">
                    <a class="active" data-bs-toggle="tab" href="#des-details1">Mô Tả</a>
                    <a data-bs-toggle="tab" href="#des-details3">Reviews ({{$votes->count()}})</a>
                </div>
                <div class="tab-content description-review-bottom">
                    <div id="des-details1" class="tab-pane active">
                        <div class="product-description-wrapper">
                            <p>{{$product -> description}}</p>
                        </div>
                    </div>
                    <div id="des-details3" class="tab-pane">
                        <div class="row">
                            <div class="col-lg-7">
                                @foreach($votes as $vote)
                                    <div class="review-wrapper">
                                    <div class="single-review">
                                        <div class="review-img">
                                            <img src="{{asset('assets/images/logo/logo_cmt2.png')}}" alt="" width="100px" />
                                        </div>
                                        <div class="review-content">
                                            <div class="review-top-wrap">
                                                <div class="review-left">
                                                    <div class="review-name">
                                                        <h4>{{\App\Models\User::find($vote->user_id)->name}}</h4>
                                                    </div>
                                                    <div class="rating-product">
                                                        @if($vote->stars ==  1)
                                                            <div style="padding: 7px;" class="stars2">
                                                                <input type="radio" id="star5" name="stars" value="5" />
                                                                <label for="star5" title="text" chec>5 stars</label>
                                                                <input type="radio" id="star4" name="stars" value="4" />
                                                                <label for="star4" title="text" >4 stars</label>
                                                                <input type="radio" id="star3" name="stars" value="3" />
                                                                <label for="star3" title="text">3 stars</label>
                                                                <input type="radio" id="star2" name="stars" value="2" />
                                                                <label for="star2" title="text">2 stars</label>
                                                                <input type="radio" id="star1" name="stars" value="1" />
                                                                <label style="color: #ffc700" for="star1" title="text">1 star</label>
                                                            </div>
                                                        @elseif($vote->stars ==  2)
                                                            <div style="padding: 7px;" class="stars2">
                                                                <input type="radio" id="star5" name="stars" value="5" />
                                                                <label for="star5" title="text" chec>5 stars</label>
                                                                <input type="radio" id="star4" name="stars" value="4" />
                                                                <label for="star4" title="text" >4 stars</label>
                                                                <input type="radio" id="star3" name="stars" value="3" />
                                                                <label for="star3" title="text">3 stars</label>
                                                                <input type="radio" id="star2" name="stars" value="2" />
                                                                <label style="color: #ffc700" for="star2" title="text">2 stars</label>
                                                                <input type="radio" id="star1" name="stars" value="1" />
                                                                <label style="color: #ffc700" for="star1" title="text">1 star</label>
                                                            </div>
                                                        @elseif($vote->stars ==  3)
                                                            <div style="padding: 7px;" class="stars2">
                                                                <input type="radio" id="star5" name="stars" value="5" />
                                                                <label for="star5" title="text" chec>5 stars</label>
                                                                <input type="radio" id="star4" name="stars" value="4" />
                                                                <label for="star4" title="text" >4 stars</label>
                                                                <input type="radio" id="star3" name="stars" value="3" />
                                                                <label style="color: #ffc700" for="star3" title="text">3 stars</label>
                                                                <input type="radio" id="star2" name="stars" value="2" />
                                                                <label style="color: #ffc700" for="star2" title="text">2 stars</label>
                                                                <input type="radio" id="star1" name="stars" value="1" />
                                                                <label style="color: #ffc700" for="star1" title="text">1 star</label>
                                                            </div>
                                                        @elseif($vote->stars ==  4)
                                                            <div style="padding: 7px;" class="stars2">
                                                                <input type="radio" id="star5" name="stars" value="5" />
                                                                <label for="star5" title="text" chec>5 stars</label>
                                                                <input type="radio" id="star4" name="stars" value="4" />
                                                                <label style="color: #ffc700" for="star4" title="text" >4 stars</label>
                                                                <input type="radio" id="star3" name="stars" value="3" />
                                                                <label style="color: #ffc700" for="star3" title="text">3 stars</label>
                                                                <input type="radio" id="star2" name="stars" value="2" />
                                                                <label style="color: #ffc700" for="star2" title="text">2 stars</label>
                                                                <input type="radio" id="star1" name="stars" value="1" />
                                                                <label style="color: #ffc700" for="star1" title="text">1 star</label>
                                                            </div>
                                                        @elseif($vote->stars ==  5)
                                                            <div style="padding: 7px;" class="stars2">
                                                                <input type="radio" id="star5" name="stars" value="5" />
                                                                <label style="color: #ffc700" for="star5" title="text" chec>5 stars</label>
                                                                <input type="radio" id="star4" name="stars" value="4" />
                                                                <label style="color: #ffc700" for="star4" title="text" >4 stars</label>
                                                                <input type="radio" id="star3" name="stars" value="3" />
                                                                <label style="color: #ffc700" for="star3" title="text">3 stars</label>
                                                                <input type="radio" id="star2" name="stars" value="2" />
                                                                <label style="color: #ffc700" for="star2" title="text">2 stars</label>
                                                                <input type="radio" id="star1" name="stars" value="1" />
                                                                <label style="color: #ffc700" for="star1" title="text">1 star</label>
                                                            </div><br/>
                                                        @endif
                                                    </div>
                                                </div>
                                                @if(auth()->id() == $vote->user_id)

                                                    <form action="{{route('user.vote.delete',$vote->id)}}" method="POST">
                                                        @method('DELETE')
                                                        @csrf
                                                        <div class="review-left">
                                                            <button type="submit"><i class="icon-trash"></i></button>
                                                        </div>
                                                    </form>
                                                @endif
                                            </div>
                                            <div class="review-bottom">
                                                <p>
                                                    {{$vote->comment}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>


                            <div class="col-lg-5">
                                <div class="ratting-form-wrapper pl-50">
                                    @if(auth()->check())
                                    <h3 style="color: #ff7004; padding-bottom: 5px">Thêm đánh giá</h3>
                                    <div class="ratting-form">
                                            <form action="{{route('user.vote.store',$product->id )}}" method="POST">
                                                @csrf
                                            <div class="star-box">
                                                <span>Đánh giá:</span>
                                                <div class="stars">
                                                    <input type="radio" id="star5" name="stars" value="5" />
                                                    <label for="star5" title="text">5 stars</label>
                                                    <input type="radio" id="star4" name="stars" value="4" />
                                                    <label for="star4" title="text">4 stars</label>
                                                    <input type="radio" id="star3" name="stars" value="3" />
                                                    <label for="star3" title="text">3 stars</label>
                                                    <input type="radio" id="star2" name="stars" value="2" />
                                                    <label for="star2" title="text">2 stars</label>
                                                    <input type="radio" id="star1" name="stars" value="1" />
                                                    <label for="star1" title="text">1 star</label>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="rating-form-style form-submit">
                                                        <textarea id="comment" name="comment" rows="4" cols="50" placeholder="Nội dung ..."></textarea>
                                                        <button class="btn btn-primary btn-hover-color-primary " type="submit" value="Submit">Submit</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- product details description area end -->
    <!-- New Product Start -->
    <div class="section pb-100px" data-aos="fade-up" data-aos-delay="200">
        <div class="container">
            <!-- section title start -->
            <div class="row">
                <div class="col-md-12">
                    <div class="section-title text-start mb-11">
                        <h2 class="title">Sản Bạn Có Thể Biết</h2>
                    </div>
                </div>
            </div>
            <!-- section title start -->
            <div class="new-product-slider swiper-container slider-nav-style-1" data-aos="fade-up" data-aos-delay="200">
                <div class="new-product-wrapper swiper-wrapper">
                    @foreach($same as $sames)
                    <!-- Single Prodect -->
                    <div class="new-product-item swiper-slide">
                        <form action="{{route('cart.addCart')}}"  method="POST">
                            @csrf
                            <input hidden name="product_id" value="{{$sames->id}}">
                        <div class="product">
                            <div class="thumb">
                                <a href="{{route('user.product.show', $sames->id)}}" class="image">
                                    <img src="{{ asset('uploads/products/'.$sames->image) }}" alt="Product" width="276px" height="324.7px" />
                                    <img class="hover-image" src="{{ asset('uploads/products/'.$sames->image) }}" alt="Product" width="276px" height="324.7px" />
                                </a>
                                <span class="badges">
                                    <span class="new">New</span>
                                    <span class="sale">-{{$sames->ticket}}%</span>
                                </span>
                                <div class="actions">
                                    <a class="action wishlist" title="Wishlist"><i
                                            class="icon-heart"></i></a>
                                    <a class="action quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#exampleModal"><i
                                            class="icon-size-fullscreen"></i></a>
                                </div>
                                <button title="Add To Cart" class=" add-to-cart">Thêm Vào Giỏ</button>
                            </div>
                            <div class="content">
                                <h5 class="title"><a
                                        href="{{route('user.product.show', $sames->id)}}">{{$sames->name}}</a>
                                </h5>
                                @if($sames->ticket > 0)
                                    <span class="price">
                                            <span class="new" style="color: #ff7004">{{$sames->price * $sames->ticket / 100}} đ</span>
                                                <span class="old">{{$sames->price}} đ</span>
                                            </span>
                                @else
                                    <span class="price">
                                            <span class="new" style="color: #ff7004">{{$sames->price}} đ</span>
                                            </span>
                                @endif
                            </div>
                        </div>
                        </form>
                    </div>
                    @endforeach
                </div>
                <!-- Add Arrows -->
                <div class="swiper-buttons">
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                </div>
            </div>
        </div>
    </div>

    <!-- New Product End -->
@endsection
