@extends('users.layouts.app')

@section('content')
    <div class="offcanvas-overlay"></div>
    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area">
        <h5 class="breadcrumb-title" style="text-align: center;">{{$categoryName->name}}</h5>
    </div>

    <!-- breadcrumb-area end -->

    <!-- Shop Category pages -->
    <div class="shop-category-area pb-100px pt-70px">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 order-lg-last col-md-12 order-md-first">
                    <!-- Shop Top Area Start -->
                    <div class="shop-top-bar d-flex" style="height: 38px">
                        <!-- Left Side start -->
                        <p>Có {{$products->count()}} kết quả.</p>
                        <!-- Left Side End -->
                    </div>
                    <!-- Shop Top Area End -->

                    <!-- Shop Bottom Area Start -->
                    <div class="shop-bottom-area">

                        <div class="row">
                            @foreach($products as $product)
                                <div class="col-lg-4  col-md-6 col-sm-6 col-xs-6" data-aos="fade-up" data-aos-delay="200">
                                    <form action="{{route('cart.addCart')}}"  method="POST">
                                        @csrf
                                        <input hidden name="product_id" value="{{$product->id}}">
                                        <div class="product mb-25px">
                                            <div class="thumb">
                                                <a href="{{route('user.product.show', $product->id)}}" class="image">
                                                    <img src="{{ asset('uploads/products/'.$product->image) }}" alt="Product" width="276px" height="324.7px"/>
                                                    <img class="hover-image" src="{{ asset('uploads/products/'.$product->image) }}" alt="Product" alt="Product" width="276px" height="324.7px"/>
                                                </a>
                                                <span class="badges">
                                            <span class="new">New</span>
                                            <span class="sale">-{{$product->ticket}}%</span>
                                        </span>
                                                <div class="actions">
                                                    <a class="action wishlist" title="Wishlist"><i
                                                            class="icon-heart"></i></a>
                                                    <a class="action quickview" data-link-action="quickview" title="Quick view" data-bs-toggle="modal" data-bs-target="#exampleModal"><i class="icon-size-fullscreen"></i></a>
                                                </div>
                                                <button title="Add To Cart" class=" add-to-cart">Thêm Vào Giỏ</button>
                                            </div>
                                            <div class="content">
                                                <h5 class="title"><a
                                                        href="{{route('user.product.show', $product->id)}}">{{$product->name}}</a>
                                                </h5>
                                                @if($product->ticket > 0)
                                                    <span class="price">
                                            <span class="new" style="color: #ff7004">{{$product->price * $product->ticket / 100}} đ</span>
                                                <span class="old">{{$product->price}} đ</span>
                                            </span>
                                                @else
                                                    <span class="price">
                                            <span class="new" style="color: #ff7004">{{$product->price}} đ</span>
                                            </span>
                                                @endif
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            @endforeach

                        </div>
                    {{$products->appends(request()->all())->Links()}}
                    <!--  Pagination Area Start -->
                    {{--                        <div class="pro-pagination-style text-center mb-md-30px mb-lm-30px mt-30px" data-aos="fade-up">--}}
                    {{--                            <ul>--}}
                    {{--                                <li>--}}
                    {{--                                    <a class="prev" href="#"><i class="ion-ios-arrow-left"></i></a>--}}
                    {{--                                </li>--}}
                    {{--                                <li><a class="active" href="#">1</a></li>--}}
                    {{--                                <li><a href="#">2</a></li>--}}
                    {{--                                <li>--}}
                    {{--                                    <a class="next" href="#"><i class="ion-ios-arrow-right"></i></a>--}}
                    {{--                                </li>--}}
                    {{--                            </ul>--}}
                    {{--                        </div>--}}
                    <!--  Pagination Area End -->
                    </div>
                    <!-- Shop Bottom Area End -->
                </div>
                <!-- Sidebar Area Start -->
                <div class="col-lg-3 order-lg-first col-md-12 order-md-last mb-md-60px mb-lm-60px">
                    <div class="shop-sidebar-wrap">
                        <!-- Sidebar single item -->
                        <div class="sidebar-widget">
                            <div class="main-heading">
                                <h3 class="sidebar-title">Danh mục</h3>
                            </div>
                            <div class="sidebar-widget-category">
                                <ul>
                                    @foreach($categories as $category)
                                        <li><a href="{{route('user.product.listCategory',$category->id)}}" class="selected">{{$category->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection
