@extends('users.layouts.app')

@section('content')
</div><div class="offcanvas-overlay"></div>
<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <h5 class="breadcrumb-title" style="text-align: center;">Giới Thiệu</h5>
</div>

<!-- breadcrumb-area end -->

<!-- About Us Area Start -->
<section class="about-area pb-100px pt-100px">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="about-left-image mb-md-30px mb-lm-30px" data-aos="fade-up">
                    <img src="http://localhost:81/MvcPdo/Mvc1/public/uploads/about/single-blog.jpg" alt="" class="img-responsive w-100" width="576px" height="333.46px" />
                </div>
            </div>
            <div class="col-lg-6">
                <div class="about-content">
                    <div class="about-title" data-aos="fade-up">
                        <h2>Welcome To HY-GGE</h2>
                    </div>
                    <p class="mb-30px" data-aos="fade-up" data-aos-delay="200">
                    <p style="text-align:justify">Với mong muốn ph&aacute;t triển thương hiệu Việt bằng nội lực, Hy-gge Store đ&atilde; ch&uacute; trọng v&agrave;o thiết kế v&agrave; sản xuất nội thất trong nước. Danh mục sản phẩm của Hy-gge Store thường xuy&ecirc;n được đổi mới v&agrave; cập nhật, li&ecirc;n tục cung cấp cho kh&aacute;ch h&agrave;ng c&aacute;c d&ograve;ng sản phẩm theo xu hướng mới nhất. Do ch&iacute;nh người Việt thiết kế v&agrave; sản xuất, nội thất thương hiệu Hy-gge Store lu&ocirc;n ph&ugrave; hợp với cuộc sống &Aacute; Đ&ocirc;ng, đem đến sự tiện nghi ho&agrave;n hảo trong mọi kh&ocirc;ng gian sống.</p>

                    <p style="text-align:justify">Hơn 70% sản phẩm của Hy-gge Store được thiết kế, sản xuất bởi đội ngũ nh&acirc;n vi&ecirc;n c&ugrave;ng c&ocirc;ng nh&acirc;n ưu t&uacute; với nh&agrave; m&aacute;y c&oacute; cơ sở vật chất hiện đại bậc nhất tại Việt Nam. Sự kh&aacute;c biệt của Hy-gge Store ch&iacute;nh l&agrave; s&aacute;ng tạo nội thất th&agrave;nh phong c&aacute;ch ri&ecirc;ng, ph&ugrave; hợp với nhu cầu kh&aacute;ch h&agrave;ng.</p>
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us Area End -->
@endsection
