@extends('users.layouts.app')

@section('content')
    <div class="offcanvas-overlay"></div>
    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area">
        <h5 class="breadcrumb-title" style="text-align: center;">Blog</h5>
    </div>

    <!-- breadcrumb-area end -->
    <!-- Blog Area Start -->
    <div class="blog-grid pb-100px pt-100px main-blog-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 order-lg-last col-md-12 order-md-first">
                    @foreach($posts as $post)
                    <div class="row mb-50px">
                        <div class="col-lg-5 col-md-6" data-aos="fade-up" data-aos-delay="200">
                            <div class="single-blog-post blog-list-post">
                                <div class="blog-post-media">
                                    <div class="blog-image">
                                        <a href="{{route('user.posts.detail', $post->id)}}"><img class="img-responsive" src="{{ asset('uploads/products/'.$post->image) }}" alt="blog" width="351px" height="300px"/></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7 col-md-6 align-self-center align-items-center" data-aos="fade-up" data-aos-delay="400">
                            <div class="blog-post-content-inner mt-lm-20px">
                                <div class="blog-athor-date">
                                    <a class="blog-date">{{ $post->created_at->format('d M')}}</a>
                                </div>
                                <h4 class="blog-title"><a href="{{route('user.posts.detail', $post->id)}}">{{$post->title}}</a></h4>
                                <p style="height: 104px">
                                    {{substr($post->description, 0, 200)}}
                                </p>
                                <a href="{{route('user.posts.detail', $post->id)}}" class="btn btn-lg btn-hover-color-primary btn-color-dark mt-25px">Read More</a>
                            </div>
                        </div>
                        <!-- single blog post -->
                    </div>
                    @endforeach
                    <!-- single blog post -->
                    <!--  Pagination Area Start -->
                        {{$posts->appends(request()->all())->Links()}}
{{--                    <div class="pro-pagination-style mt-20px mb-md-50px mb-lm-60px text-center" data-aos="fade-up" data-aos-delay="200">--}}
{{--                        <div class="pages">--}}
{{--                            {{$posts->appends(request()->all())->Links()}}--}}
{{--                            <ul>--}}
{{--                                {{$posts->appends(request()->all())->Links()}}--}}
{{--                                <li class="li"><a class="page-link" href="#"><i class="ion-ios-arrow-left"></i></a></li>--}}
{{--                                <li class="li"><a class="page-link active" href="#">1</a></li>--}}
{{--                                <li class="li"><a class="page-link" href="#">2</a></li>--}}
{{--                                <li class="li"><a class="page-link" href="#"><i class="ion-ios-arrow-right"></i></a></li>--}}
{{--                            </ul>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    <!--  Pagination Area End -->
                </div>
                <!-- Sidebar Area Start -->
                <div class="col-lg-3 order-lg-first col-md-12 order-md-last mt-md-50px mt-lm-50px" data-aos="fade-up" data-aos-delay="200">
                    <div class="left-sidebar shop-sidebar-wrap">
                        <!-- Sidebar single item -->
                        <div class="sidebar-widget mt-40px">
                            <h3 class="sidebar-title">Instagram </h3>
                            <div class="flicker-widget">
                                <ul>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/1.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/2.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/3.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/4.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/1.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/2.png')}}" alt="instagram"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Sidebar single item -->
                    </div>
                </div>
                <!-- Sidebar Area End -->
            </div>
        </div>
    </div>
    <!-- Blag Area End -->
@endsection

