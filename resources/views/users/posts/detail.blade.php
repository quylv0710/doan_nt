@extends('users.layouts.app')

@section('content')
    <div class="offcanvas-overlay"></div>
    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area">
        <h5 class="breadcrumb-title" style="text-align: center;">Chi tiết bài viết</h5>
    </div>

    <!-- breadcrumb-area end -->
    <!-- Blog Area Start -->
    <div class="blog-grid pb-100px pt-100px main-blog-page single-blog-page">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-12">
                    <div class="blog-posts ">
                        <div class="single-blog-post blog-grid-post">
                            <div class="blog-post-content-inner mt-30px" data-aos="fade-up" data-aos-delay="200">
                                <h4 class="blog-title"><a>{{$post->title}}</a></h4>
                                <div class="blog-athor-date">
                                    <a class="blog-date">{{ $post->created_at->format('d M')}}</a>
                                </div>
                                <p>
                                    {!! $post->content !!}
                                </p>
                            </div>
                        </div>
                        <!-- single blog post -->
                    </div>
                    <div class="blog-single-tags-share d-sm-flex justify-content-between">
                        <div class="blog-single-tags d-flex mb-xs-15px" data-aos="fade-up" data-aos-delay="200">
                            <span class="title">Tags: </span>
                            <ul class="tag-list">
                                <li><a href="#">Food,</a></li>
                                <li><a href="#">Water,</a></li>
                                <li><a href="#">Shelter</a></li>
                            </ul>
                        </div>
                        <div class="blog-single-share d-flex" data-aos="fade-up" data-aos-delay="200">
                            <span class="title">Share:</span>
                            <ul class="social">
                                <li>
                                    <a href="#"><i class="icon-social-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="icon-social-twitter"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="icon-social-google"></i></a>
                                </li>
                                <li>
                                    <a href="#"><i class="icon-social-instagram"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="blog-related-post">
                        <div class="row">
                            <div class="col-md-12 text-center" data-aos="fade-up">
                                <!-- Section Title -->
                                <div class="section-title">
                                    <h2 class="title m-0">Bài Viết Quan Tâm</h2>
                                </div>
                                <!-- Section Title -->
                            </div>
                        </div>
                        <div class="row">
                            @foreach($sames as $same)
                                @if($loop->first)
                            <div class="col-md-4 mb-lm-30px related-post-item" data-aos="fade-up" data-aos-delay="200">
                                <div class="blog-post-media">
                                    <div class="blog-image single-blog">
                                        <a href="{{route('user.posts.detail', $post->id)}}"><img class="img-responsive" src="{{ asset('uploads/products/'.$same->image) }}" alt="blog" width="351px" height="300px"/></a>
                                    </div>
                                </div>
                                <div class="blog-post-content-inner mt-3">
                                    <div class="blog-athor-date">
                                        <a class="blog-date" href="#">{{ $post->created_at->format('d M')}}</a>
                                    </div>
                                    <h4 class="blog-title"><a href="{{route('user.posts.detail', $same->id)}}">{{$same->title}}</a></h4>
                                    <p style="height: 104px">
                                        {{substr($same->description, 0, 200)}}
                                    </p>
                                    <a href="{{route('user.posts.detail', $same->id)}}" class="btn btn-lg btn-hover-color-primary btn-color-dark mt-25px">Read More</a>
                                </div>
                            </div>
                                @endif
                            @endforeach
                                @foreach($sames as $same)
                                    @if($loop->iteration == 2)
                            <div class="col-md-4 mb-lm-30px related-post-item" data-aos="fade-up" data-aos-delay="400">
                                <div class="blog-post-media">
                                    <div class="blog-image single-blog">
                                        <a href="{{route('user.posts.detail', $post->id)}}"><img class="img-responsive" src="{{ asset('uploads/products/'.$same->image) }}" alt="blog" width="351px" height="300px"/></a>
                                    </div>
                                </div>
                                <div class="blog-post-content-inner mt-3">
                                    <div class="blog-athor-date">
                                        <a class="blog-date" href="#">{{ $post->created_at->format('d M')}}</a>
                                    </div>
                                    <h4 class="blog-title"><a href="{{route('user.posts.detail', $same->id)}}">{{$same->title}}</a></h4>
                                    <p style="height: 104px">
                                        {{substr($same->description, 0, 200)}}
                                    </p>
                                    <a href="{{route('user.posts.detail', $same->id)}}" class="btn btn-lg btn-hover-color-primary btn-color-dark mt-25px">Read More</a>
                                </div>
                            </div>
                                    @endif
                                @endforeach
                                @foreach($sames as $same)
                                    @if($loop->last)
                            <div class="col-md-4 related-post-item" data-aos="fade-up" data-aos-delay="600">
                                <div class="blog-post-media">
                                    <div class="blog-image single-blog">
                                        <a href="{{route('user.posts.detail', $post->id)}}"><img class="img-responsive" src="{{ asset('uploads/products/'.$same->image) }}" alt="blog" width="351px" height="300px"/></a>
                                    </div>
                                </div>
                                <div class="blog-post-content-inner mt-3">
                                    <div class="blog-athor-date">
                                        <a class="blog-date" href="#">{{ $post->created_at->format('d M')}}</a>
                                    </div>
                                    <h4 class="blog-title"><a href="{{route('user.posts.detail', $same->id)}}">{{$same->title}}</a></h4>
                                    <p style="height: 104px">
                                        {{substr($same->description, 0, 200)}}
                                    </p>
                                    <a href="{{route('user.posts.detail', $same->id)}}" class="btn btn-lg btn-hover-color-primary btn-color-dark mt-25px">Read More</a>
                                </div>
                            </div>
                                    @endif
                                @endforeach
                        </div>
                    </div>
                </div>
                <!-- Sidebar Area Start -->
                <div class="col-lg-3 col-md-12  mt-md-50px mt-lm-50px" data-aos="fade-up" data-aos-delay="200">
                    <div class="left-sidebar shop-sidebar-wrap">
                        <!-- Sidebar single item -->
                        <div class="sidebar-widget mt-40px">
                            <h3 class="sidebar-title">Instagram </h3>
                            <div class="flicker-widget">
                                <ul>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/1.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/2.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/3.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/4.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/1.png')}}" alt="instagram"></a>
                                    </li>
                                    <li>
                                        <a class="image-link" target="_blank"><img src="{{asset('assets/images/instragram-image/2.png')}}" alt="instagram"></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- Sidebar single item -->
                    </div>
                </div>
                <!-- Sidebar Area End -->
            </div>
        </div>
    </div>
    <!-- Blag Area End -->
@endsection
