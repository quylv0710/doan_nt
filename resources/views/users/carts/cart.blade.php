@extends('users.layouts.app')

@section('content')
    <div class="offcanvas-overlay"></div>
    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area">
        <h5 class="breadcrumb-title" style="text-align: center;">Giỏ hàng</h5>
    </div>

    <!-- breadcrumb-area end -->

    <!-- Cart Area Start -->
    <div class="cart-main-area pt-100px pb-100px">
        <div class="container">
            <h3 class="cart-page-title" style="color: #FF7004;">GIỎ HÀNG CỦA BẠN</h3>
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="table-content table-responsive cart-table-content">
                            <table>
                                <thead>
                                <tr>
                                    <th>Hình ảnh</th>
                                    <th>Tên sản phẩm</th>
                                    <th>Giá</th>
                                    <th>Giảm giá</th>
                                    <th>Số lượng</th>
                                    <th>Số tiền</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($carts as $cart)
                                    <form action="{{ route('cart.updateCart', $cart->id) }}" method="POST">
                                        @method('PUT')
                                        @csrf
                                        <tr>
                                            <td class="product-thumbnail">
                                                <a href="#"><img class="img-responsive ml-15px" src="{{ asset('uploads/products/'.$cart->image) }}" alt="" /></a>
                                            </td>
                                            <td class="product-name"><a href="#">{{$cart->name}}</a></td>
                                            <td class="product-price-cart"><span class="amount">{{number_format($cart->price)}}đ</span></td>
                                            <td class="product-price-cart"><span class="amount">{{number_format($cart->ticket)}}%</span></td>
                                            <td class="product-quantity">
                                                <div class="cart-plus-minus">
                                                    <input class="cart-plus-minus-box" type="number" id="quantity" name="quantity" min="1" max="50" value="{{$cart->pivot->count}}"/>
                                                </div>
                                            </td>
                                            <td class="product-subtotal">{{number_format($cart->pivot->sum_price)}}đ</td>
                                            <td class="product-remove">
                                                <button type="submit"><i class="icon-pencil"></i></button>
                                    </form>
                                                <form action="{{ route('cart.deleteCart', $cart->id) }}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                    <button type="submit"><i class="icon-close"></i></button>
                                                </form>
        {{--                                        <a href="#"><i class="icon-close"></i></a>--}}
                                            </td>
                                        </tr>

                                @endforeach

                                </tbody>
                            </table>
                        </div>
                        <div class="row" >
                            <div class="money" style="position: relative;">
                                <div class="makemn" style="text-align: right;width:calc(100% - 23px);;position: absolute; right: 11px;padding-top: 8px;">
                                    <h4 class="tien" style="padding-top: 10px;margin-right: 24px; color: #FF7004; font-style: normal;"><b>Thành tiền:
                                        {{number_format($sumCart)}} đ</b>
                                    </h4>
                                </div>
                            </div>
                            <div class="col-lg-12" style="margin-top: 20px;">
                                <div class="cart-shiping-update-wrapper">
                                </div>
                            </div>
                        </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 mb-lm-30px">
                            <div class="discount-code-wrapper">
                                <form action="{{ route('orders') }}" method="POST">
                                    @csrf
                                    @foreach($carts as $cart)
                                        <input hidden name="product_id" value="{{$cart->id}}">
                                    @endforeach
                                    <input hidden name="sum_price" value="{{$sumCart}}">
                                    <input hidden name="status" value="1">
                                    <input hidden name="start" value="{{date('Y-m-d H:i:s')}}">
                                    <input hidden name="end" value="{{date('Y-m-d H:i:s')}}">

                                    <div class="title-wrap">
                                        <h4 class="cart-bottom-title section-bg-gray">Thông tin đặt hàng</h4>
                                    </div>
                                    <div class="tax-select mb-25px pt-3">
                                        <label>Người đặt </label>
                                        <input style="background-color: white" type="text" disabled value="{{auth()->user()->name}}" />
                                    </div>
                                    <div class="tax-select mb-25px pt-3">
                                        <label>Địa chỉ nhận hàng </label>
                                        <input style="background-color: white" type="text" required="" name="address" />
                                    </div>
                                    <h4 class="grand-totall-title">Tổng tiền: <span style="color: #ff7004">{{number_format($sumCart)}} đ</span></h4>
                                    <div class="discount-code " >
                                            <button class="cart-btn-2 col-lg-12" type="submit">Đặt hàng</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Cart Area End -->
@endsection
