@extends('admin.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Roles</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Roles</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Update Role</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{route('roles.update', $role->id)}}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="text" class="form-control" name="name" value="{{$role->name}}">
                                        <p>
                                        @error('name')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Display Name</label>
                                        <input type="text" class="form-control" name="display_name"
                                               value="{{$role->display_name}}">
                                        <p>
                                        @error('display_name')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input select-all-permission" type="checkbox"
                                               id="checkAll">
                                        <label class="form-label">Permission</label>
                                        <div class="row">
                                            @foreach($permissionGroup as $group => $permission)
                                                <div class="col car">
                                                    @foreach($permission as $key => $permissionItem)
                                                        <div class="card-text">
                                                            <div class="form-check">
                                                                <input
                                                                    {@foreach($role->permissions as $item)
                                                                    {{ $item->id == $permissionItem->id ? 'checked' : '' }}
                                                                    @endforeach
                                                                    class="b"
                                                                    type="checkbox" id="checkAllb"
                                                                    name="permission_name[]"
                                                                    value="{{ $permissionItem->id }}">
                                                                <label class="form-check-label"
                                                                       for="flexCheckDefault">
                                                                    {{ $permissionItem->display_name }}
                                                                </label>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

