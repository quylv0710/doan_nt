@extends('admin.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>User</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('users.index')}}">Users</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col-md-12">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Edit User</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{route('users.update', $user->id)}}" method="POST">
                                @method('PUT')
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Name</label>
                                        <input type="text" class="form-control" name="name" value="{{$user->name}}">
                                        <p>
                                        @error('name')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Email</label>
                                        <input type="text" class="form-control" name="email"
                                               value="{{$user->email}}">
                                        <p>
                                        @error('email')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Birthday</label>
                                        <input type="text" class="form-control" name="birthday"
                                               value="{{$user->birthday}}">
                                        <p>
                                        @error('birthday')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Address</label>
                                        <textarea type="text" class="form-control" name="address"
                                                  value="">{{$user->address}}</textarea>
                                        <p>
                                        @error('address')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Phone Number</label>
                                        <input type="text" class="form-control" name="phone_number"
                                               value="{{$user->phone_number}}">
                                        <p>
                                        @error('phone_number')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-label">Gender</label>
                                        <div>
                                            <input type="radio" name="gender" value="0"
                                                   {{$user->gender == "0" ? 'checked' : ''}} checked> Male<br>
                                            <input type="radio" name="gender"
                                                   value="1"{{$user->gender == "1" ?'checked' : '' }}> Female<br>
                                        </div>
                                    </div>
{{--                                    <div class="form-group">--}}
{{--                                        <label for="exampleInputPassword1">Password</label>--}}
{{--                                        <input type="text" class="form-control" name="password"--}}
{{--                                               value="">--}}
{{--                                        <p>--}}
{{--                                        @error('password')--}}
{{--                                        <div class="link-danger">{{ $message }}</div>--}}
{{--                                        @enderror--}}
{{--                                        </p>--}}
{{--                                    </div>--}}
                                    <div class="form-check">
                                        <input class="form-check-input select-all-permission" type="checkbox"
                                               id="checkAll">
                                        <label class="form-label">Role</label>
                                        <div class="row">
                                            @foreach($roles as $role)
                                                <div class="row">
                                                    <div class="form-check">
                                                        <input @foreach($user->roles as $item)
                                                               {{$item->id == $role->id ? 'checked' :''}}
                                                               @endforeach
                                                               class="b" type="checkbox"
                                                               name="role_name[]"
                                                               value="{{ $role->id }}">
                                                        <label class="form-check-label" for="flexCheckDefault">
                                                            {{ $role->display_name }}
                                                        </label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection


