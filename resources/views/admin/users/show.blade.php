@extends('admin.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Show Roles</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('users.index')}}">Roles</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- ./col -->
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-2">ID</dt>
                                    <dd class="col-sm-10">{{$user->id}}</dd>
                                    <dt class="col-sm-2">Name</dt>
                                    <dd class="col-sm-10">{{$user->name}}</dd>
                                    <dt class="col-sm-2">Email</dt>
                                    <dd class="col-sm-10">{{$user->email}}</dd>
                                    <dt class="col-sm-2">Birthday</dt>
                                    <dd class="col-sm-10">{{$user->birthday}}</dd>
                                    <dt class="col-sm-2">Address</dt>
                                    <dd class="col-sm-10">{{$user->address}}</dd>
                                    <dt class="col-sm-2">Phone</dt>
                                    <dd class="col-sm-10">{{$user->phone_number}}</dd>
                                    <dt class="col-sm-2">Sex</dt>
                                    <dd class="col-sm-10"> {{ $user->gender == 0 ? 'male' : 'female' }}</dd>
                                    <dt class="col-sm-2">Roles</dt>
                                    <dd class="col-sm-10">
                                        @foreach($user->roles as $role)
                                            <span style="font-size: 12px"
                                                  class="badge badge-success">{{ $role->display_name }}</span>
                                        @endforeach
                                    </dd>
                                </dl>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.container-fluid -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
