@extends('admin.layouts.app')
@php
    $stt = (($_GET['page'] ?? 1) - 1) * 5;
@endphp
@section('content')
    <div class="content-wrapper" id="something">
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Category</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Category</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <div id="list" data-action="{{route('categories.list')}}">
            <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <th style="width: 5%">ID</th>
                        <th>Name</th>
                        <th>Parent Name</th>
                        <th style="width: 15%">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($data as $category)
                        <tr id="xid{{$category->id}}">
                            <td>{{++$stt}}</td>
                            <td>{{$category->name}}</td>
                            <td>{{ $category->parent->name ?? ""}}</td>
                            <td>
                                @hasPermission('category_edit')
                                <a href="{{route('categories.edit', $category->id)}}"
                                   class="badge bg-success"><i class="fas fa-edit"></i></a>
                                @endhasPermission
                                <a>
                                    <button data-id="{{$category->id}}"
                                            class=" deleteCategory badge bg-danger btn-delete">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <p>
                    {{$data->appends(request()->all())->Links()}}
                </p>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection

