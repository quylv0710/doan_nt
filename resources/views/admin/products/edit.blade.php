<form id="editProductForm" data-action="{{route('products.update', $product->id)}}" name="ProductForm"
      method="POST"
      accept-charset="UTF-8"
      enctype="multipart/form-data">
    <input type="hidden" id="Eid">
    <div class="form-group">
        <label for="recipient-name" class="col-form-label">Name:</label>
        <input type="text" name="name"  value="{{$product->name}}" class="form-control" id="name">
        <div class="text-danger form-check errors rs-errors error-name"></div>
    </div>
    <div class="form-group">
        <label for="message-text" class="col-form-label">Price:</label>
        <input type="text" name="price" value="{{$product->price}}" class="form-control" id="price">
        <div class="text-danger form-check errors rs-errors error-price"></div>
    </div>
    <div class="form-group">
        <label for="recipient-name" class="col-form-label">Description:</label>
        <input type="text" name="description" value="{{$product->description}}" class="form-control" id="description">
        <div class="text-danger form-check errors rs-errors error-description"></div>
    </div>
    <div class="form-group">
        <div class="mb-3">
            <label class="form-label">Category</label>
            <select class="form-select" name="category_id[]" aria-label="Default select example">
                @foreach($categories as $category)
                    <option {{$product->categories->contains('id',$category->id) ? 'selected' : ''}} value="{{ $category->id }}">{{ $category->name }}</option>
                @endforeach
                <option value="">Select Category</option>
            </select>
        </div>
        <div class="text-danger form-check errors error-category_id"></div>
    </div>
    <div class="form-group">
        <label class="form-label">Image</label>
        <div class="form-row">
            <div class="col-10">
                <input name="image" id="imageEdit"
                       value="{{ $product->image }}" alt="{{ $product->image }}"
                       type="file" class="form-control image" >
            </div>
            <div class="col-2">
                <button id="delete_button" class=" btn btn-secondary" type="reset" ><i class="fa fa-trash" style="width: 50px"></i></button>
            </div>
        </div>
        <div class="show-image" >
            <img  src="{{ \Illuminate\Support\Facades\URL::to('uploads/products/' . $product->image) }}" width="150px"  height="100px" id="showImageEdit" alt="Ảnh">
        </div>
        <p>
        @error('image')
        <div class="link-danger">{{ $message }}</div>
        @enderror
        </p>
        <div class="text-danger form-check errors error-image"></div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" id="btn-edit-product" class="btn btn-primary">Update</button>
    </div>
</form>


