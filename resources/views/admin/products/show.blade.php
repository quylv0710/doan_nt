
<div class="container-fluid">
    <div class="row">
        <!-- ./col -->
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">
                    </h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <dl class="row">
                        <dt>ID</dt>
                        <dd>{{$product->id}}</dd>
                        <dt>Name</dt>
                        <dd>{{$product->name}}</dd>
                        <dt>Price</dt>
                        <dd>{{$product->price . " ". "VNĐ"}}</dd>
                        <dt>Description</dt>
                        <dd>{{$product->description}}</dd>
                        <dt>Image</dt>
                        <dd>
                            <img src="{{asset('uploads/products/'.$product->image)}}" width="240px" alt="">
                        </dd>
                    </dl>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <!-- ./col -->
    </div>
    <!-- /.container-fluid -->
</div>
