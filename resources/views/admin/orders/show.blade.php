@extends('admin.layouts.app')
@php
    $stt = (($_GET['page'] ?? 1) - 1) * 5;
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Roles</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Show</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                    <!-- /.card-header -->
                        <div class="card-header" style="padding-left: 20%; background: #f3f8fa ">
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 10%">Mã sản phẩm</th>
                                    <th style="width: 10%">Tên sản phẩm</th>
                                    <th style="width: 15%">Số lượng</th>
                                    <th style="width: 15%">Giá </th>
                                    <th style="width: 15%">Khuyến mãi </th>
                                    <th style="width: 15%">Thành tiền</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{$products->id}}</td>
                                        <td>{{$products->name}}</td>
                                        @if($products->ticket>0)
                                            <td>{{$product->sum_price / $products->price* 100 / $products->ticket}}</td>
                                        @else
                                            <td>{{$product->sum_price / $products->price}}</td>
                                        @endif

                                        <td>{{$products->price}}</td>
                                        <td>{{$products->ticket}}</td>
                                        <td>{{$product->sum_price}}</td>
{{--                                        <td>{{$products}}</td>--}}
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

