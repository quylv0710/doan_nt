@extends('admin.layouts.app')
@php
    $stt = (($_GET['page'] ?? 1) - 1) * 5
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Users</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card-header clearfix">
                            <a href="{{route('users.create')}}" type="button" class="btn btn-primary float-left"><i
                                    class="fas fa-plus"></i> Create</a>
                        </div>

                        @if ($message = Session::get('message'))
                            <div class="alert alert-success">
                                <p class="text-white">{{ $message }}</p>
                            </div>
                        @endif
                        <!-- /.card -->
                        <div class="card">
                            <div class="card-header" style="padding-left: 20%; background: #f3f8fa ">
                                <div class="col-md-10" style="margin-top: 5px; margin-bottom: 10px">
                                </div>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="example1" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th style="width: 3%">ID</th>
                                        <th style="width: 15%">Họ tên</th>
                                        <th style="width: 20%">Sản phẩm</th>
                                        <th style="width: 10%">Thành tiền</th>
                                        <th style="width: 20%">Địa chỉ nhận hàng</th>
                                        <th style="width: 10%">Số điện thoại</th>
                                        <th style="width: 15%">Trạng thái</th>
                                        <th style="width: 5%">action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users as $user)
                                        <form action="{{route('orders.update', $user->id)}}" method="POST">
                                            @method('PUT')
                                            @csrf
                                            <input hidden name="product_id" value="{{$user->id}}">
                                            <input hidden name="sum_price" value="{{$user->sum_price}}">
                                            <input hidden name="start" value="{{$user->start}}">
                                            <input hidden name="end" value="{{$user->end}}">
                                            <tr>
                                                <td>{{$user->id}}</td>
                                                <td>{{\App\Models\User::find($user->user_id)->name}}</td>
                                                <td>{{\App\Models\Product::find($user->product_id)->name}}</td>
                                                <td>{{number_format($user->sum_price)}} đ</td>
                                                <td>{{\App\Models\User::find($user->user_id)->address}}</td>
                                                <td>{{\App\Models\User::find($user->user_id)->phone_number}}</td>
                                                <td>
                                                        <select name="status" class="form-control">
                                                            @if($user->status == 1)
                                                                <option value="1"  selected>Chờ xác nhận</option>
                                                                <option value="2" >Đang giao hàng</option>
                                                                <option value="3" >Hoàn thành</option>
                                                            @endif
                                                            @if($user->status == 2)
                                                                    <option value="1" >Chờ xác nhận</option>
                                                                    <option value="2"  selected>Đang giao hàng</option>
                                                                    <option value="3" >Hoàn thành</option>
                                                            @endif
                                                            @if($user->status == 3)
                                                                    <option value="1"  selected>Chờ xác nhận</option>
                                                                    <option value="2">Đang giao hàng</option>
                                                                    <option value="3" selected>Hoàn thành</option>
                                                            @endif
                                                        </select>
                                                </td>
                                                <td>
                                                            <button tyle="submit">
                                                                <i class="fas fa-edit"></i>
                                                            </button>
                                                    <a href="{{route('order.show',$user->id) }}"><i class="far fa-eye"></i></a>
                                                </td>
                                            </tr>
                                        </form>
                                    @endforeach
                                    </tbody>
                                </table>
                                {{$users->appends(request()->all())->Links()}}
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>

    <div class="modal fade" id="showProduct" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Show Product</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <section class="content">
                        <div id="detail-product"></div>
                    </section>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $("table")
                .tablesorter({widthFixed: true, widgets: ['zebra']})
                .tablesorterPager({container: $("#pager")});
        });
    </script>
@endsection
