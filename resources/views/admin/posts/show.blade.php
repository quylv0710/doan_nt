@extends('admin.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Show Roles</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active"><a href="{{route('users.index')}}">Roles</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- ./col -->
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">
                                </h3>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <dl class="row">
                                    <dt class="col-sm-2">ID</dt>
                                    <dd class="col-sm-10">{{$post->id}}</dd>
                                    <dt class="col-sm-2">title</dt>
                                    <dd class="col-sm-10">{{$post->title}}</dd>
                                    <dt class="col-sm-2">description</dt>
                                    <dd class="col-sm-10">{{$post->description}}</dd>
                                    <dt class="col-sm-2">Content</dt>
                                    <dt class="col-sm-10">{!! $post->content !!}</dt>
                                    <dt class="col-sm-2">Category</dt>
                                    <dt class="col-sm-10">
                                        @foreach($categories as $category)
                                            @if($category->id == $post->category_id)
                                                {{ $category->name }}
                                            @endif
                                        @endforeach
                                    </dt>
                                </dl>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- ./col -->
                </div>
                <!-- /.container-fluid -->
            </div>
        </section>
        <!-- /.content -->
    </div>
@endsection
