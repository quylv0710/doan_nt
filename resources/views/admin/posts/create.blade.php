@extends('admin.layouts.app')

{{--@section('title', 'Dashboard')--}}

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Roles</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item"><a href="{{route('posts.index')}}">Roles</a></li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <!-- left column -->
                    <div class="col">
                        <!-- general form elements -->
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Add Post</h3>
                            </div>
                            <!-- /.card-header -->
                            <!-- form start -->
                            <form action="{{route('posts.store')}}" method="POST">
                                @csrf
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Title</label>
                                        <input type="text" class="form-control" value="{{old('title')}}" name="title" placeholder="Title . . .">
                                        <p>
                                        @error('title')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label class="exampleInputEmail1">Image</label>
                                        <div class="form-row">
                                            <div class="col-11">
                                                <input name="image" id="image" type="file" class="form-control" >
                                            </div>
                                            <div class="col-1">
                                                <button id="delete_button" class=" btn btn-secondary" type="reset" ><i class="fa fa-trash" style="width: 65px"></i></button>
                                            </div>
                                        </div>
                                        <p>
                                        @error('image')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <img id="showImage"
                                             alt=""
                                             style="width: 300px;">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Description</label>
                                        <input type="text" class="form-control" value="{{old('description')}}" name="description"
                                               placeholder="Description . . .">
                                        <p>
                                        @error('description')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Content</label>
                                            <textarea class="form-control" id="ckeditor" value="{{old('content')}}" name="content" placeholder="Content . . ." rows="9" cols="50"></textarea>
                                        <p>
                                        @error('content')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>
                                    <div class="form-group">
                                        <div class="mb-3">
                                            <label class="form-label">Category</label>
                                            <select class="form-select" name="category_id" aria-label="Default select example">
                                                <option selected value="">Select Category</option>
                                                @foreach($categories as $category)
                                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <p>
                                        @error('category_id')
                                        <div class="link-danger">{{ $message }}</div>
                                        @enderror
                                        </p>
                                    </div>

                                    <!-- Four equal columns -->
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.card -->

                    </div>
                    <!--/.col (left) -->
                    <!-- right column -->
                    <!--/.col (right) -->
                </div>
                <!-- /.row -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#image').change(function (e) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#showImage').attr('src', e.target.result);
                }
                reader.readAsDataURL(e.target.files['0']);
            })
        })

        $(document).on('click', '.btn-secondary', function () {
            $('#showImage').attr('src', '');
            $('#showImage').val('');
        })
    </script>
@endpush

