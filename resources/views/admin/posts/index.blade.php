@extends('admin.layouts.app')
@php
    $stt = (($_GET['page'] ?? 1) - 1) * 5;
@endphp
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Roles</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
                            <li class="breadcrumb-item active">Roles</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card-header clearfix">
                            <a href="{{route('post.create')}}" type="button" class="btn btn-primary float-left"><i
                                    class="fas fa-plus"></i> Create</a>
                        </div>
                        <!-- /.card -->
                        @if ($message = Session::get('message'))
                            <div class="alert alert-success">
                                <p class="text-white">{{ $message }}</p>
                            </div>
                    @endif
                    <!-- /.card-header -->
                        <div class="card-header" style="padding-left: 20%; background: #f3f8fa ">
                            <div class="col-md-10" style="margin-top: 5px; margin-bottom: 10px">
                                <form method="GET" class="m-md-3 d-flex" style="padding-right: 20%">
                                    <input type="search" name="name" value="{{ request('name') }}"
                                           class="form-control"
                                           placeholder="search ...">
                                    <button type="submit" class="btn btn-primary" style="margin-left: 7px">
                                        search
                                    </button>
                                </form>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th style="width: 5%">ID</th>
                                    <th>title</th>
                                    <th style="width: 10%; height:10%">Image</th>
                                    <th>Description</th>
                                    <th style="width: 10%">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                    <tr>
                                        <td>{{++$stt}}</td>
                                        <td>{{$post->title}}</td>
                                        <td>
                                            <img src="{{ asset('uploads/products/'.$post->image) }}" width="80px" alt="">
                                        </td>
                                        <td>{{$post->description}}</td>
                                        <td>
                                            <a href="{{route('posts.show', $post->id)}}"
                                               class="badge bg-success"><i class="far fa-eye"></i></a>
                                            <a href="{{route('posts.edit', $post->id)}}"
                                               class="badge bg-yellow"><i class="fas fa-edit"></i></a>
                                            <a  title="Delete" >
                                                <button  data-id="{{$post->id}}"
                                                         type="submit" class="badge bg-danger dele-post-btn" onclick="">
                                                    <i class="fas fa-trash"></i>
                                                </button>
                                                <form id="delete-post-form-{{$post->id}}"
                                                      action="{{route('posts.destroy', $post->id)}}" method="POST">
                                                    @method('DELETE')
                                                    @csrf
                                                </form>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <p>
                                {{$posts->appends(request()->all())->Links()}}
                            </p>

                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

