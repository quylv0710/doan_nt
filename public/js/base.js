$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

const Base = (function () {
    let modules = {};

    modules.callApiData = function (url, data = {}, method = 'get') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
            contentType: false,
            processData: false,
        })
    }

    modules.callApiNormally = function (url, data = {}, method = 'get') {
        return $.ajax({
            url: url,
            data: data,
            method: method,
        })
    }

    modules.checkGroup = function (classParent, classChildrent, e) {
        e.parents(classParent).find(classChildrent).prop('checked', e.prop('checked'));
    }

    return modules;
}(window.jQuery, window, document))

const alertMessage = (function () {
    let modules = {};

    modules.resetError = function (arr) {
        $('.rs-errors').html('');
    };

    modules.alertSuccess = function (message) {
        Swal.fire({
            position: 'top-end',
            icon: 'success',
            title: message,
            showConfirmButton: false,
            timer: 1500
        })
    }
    return modules;
}(window.jQuery, window, document))

$(document).ready(function () {
    $("#checkAll").change(function () {
        $("input:checkbox").prop('checked', $(this).prop("checked"));
    });

    $('.b').on('click', function () {
        if ($('.b:checked').length == $('.b').length) {
            $('#checkAll').prop('checked', true);
        } else {
            $('#checkAll').prop('checked', false);
        }
        if ($('.b:checked').length == $('.b').length) {
            $('.checkbox_wrapper').prop('checked', true);
        } else {
            $('.checkbox_wrapper').prop('checked', false);
        }
    });

    $('.checkbox_wrapper').on('click', function () {
        Base.checkGroup('.car', '.b', $(this));
    });
});
