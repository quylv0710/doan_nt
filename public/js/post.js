$(".dele-post-btn").click(function (){
    return new Promise((resolve, reject) => {
        swal.fire({
            title: `Are you sure delete ?`,
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
            .then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    $('#delete-post-form-' + $(this).data('id')).submit();
                    alertMessage.alertSuccess('delete success');
                } else {
                    reject(false);
                }
            })
    })
});
