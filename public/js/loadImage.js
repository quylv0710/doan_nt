const loadImage = (function () {
    let modules = {};

    modules.showImage = function (e) {
        let file = e.get(0).files[0];
        if (file) {
            let reader = new FileReader();
            reader.onload = function () {
                $('.show-image').html('').append('<img src="' + reader.result + '" width="200" height="100">');
            }
            reader.readAsDataURL(file);
        }
    };

    modules.deleteImage = function () {
        $('.show-image').html('').css('border', '0');
    };

    return modules;
}(window.jQuery, window, document));

$(document).ready(function () {
    $(document).on('change', '#image', function () {
        loadImage.showImage($(this));
    });

    $(document).on('change', '#imageEdit', function () {
        loadImage.showImage($(this));
    });
})
