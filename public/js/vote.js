$(".delete-vote-btn").click(function (){
    return new Promise((resolve, reject) => {
        swal.fire({
            title: `Are you sure delete ?`,
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        })
            .then((result) => {
                if (result.isConfirmed) {
                    event.preventDefault();
                    $('#delete-vote-form-' + $(this).data('id')).submit();
                }
                else {
                    reject(false);
                }
            })
    })
});
