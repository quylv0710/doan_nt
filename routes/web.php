<?php

use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\PostController;
use App\Http\Controllers\User\ProductController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', function () {
    return view('admin.home.dashboard');
})->name('home')->middleware('redictrouter');

Route::get('', function () {
    return redirect('/login');
})->name('redictLogin');

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::prefix('roles')->name('roles.')->group(function(){
    Route::get('/', [\App\Http\Controllers\RoleController::class, 'index'])
        ->name('index')
        ->middleware('permission:role_view','auth');

    Route::get('/create', [\App\Http\Controllers\RoleController::class, 'create'])
        ->name('create')
        ->middleware('permission:role_create','auth');

    Route::post('/store', [\App\Http\Controllers\RoleController::class, 'store'])
        ->name('store')
        ->middleware('permission:role_store','auth');

    Route::get('/{id}', [\App\Http\Controllers\RoleController::class, 'show'])
        ->name('show')
        ->middleware('permission:role_show','auth');

    Route::get('/edit/{id}', [\App\Http\Controllers\RoleController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:role_edit','auth');

    Route::put('/update/{id}', [\App\Http\Controllers\RoleController::class, 'update'])
        ->name('update')
        ->middleware('permission:role_update','auth');

    Route::delete('/delete/{id}', [\App\Http\Controllers\RoleController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:role_delete','auth');
});

//-----------------------------User----------------------------------
Route::prefix('users')->name('users.')->group(function() {
    Route::get('/', [\App\Http\Controllers\UserController::class, 'index'])
        ->name('index')
        ->middleware('permission:user_view', 'auth');

    Route::get('/create', [\App\Http\Controllers\UserController::class, 'create'])
        ->name('create')
        ->middleware('permission:user_create', 'auth');

    Route::get('/{id}', [\App\Http\Controllers\UserController::class, 'show'])
        ->name('show')
        ->middleware('permission:user_show', 'auth');

    Route::delete('/delete/{id}', [\App\Http\Controllers\UserController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:user_delete', 'auth');

    Route::get('/edit/{id}', [\App\Http\Controllers\UserController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:user_edit', 'auth');

    Route::post('/store', [\App\Http\Controllers\UserController::class, 'store'])
        ->name('store')
        ->middleware('permission:user_store', 'auth');

    Route::put('/update/{id}', [\App\Http\Controllers\UserController::class, 'update'])
        ->name('update')
        ->middleware('permission:user_update', 'auth');
});
Route::post('/orders',[\App\Http\Controllers\UserController::class, 'orders'])
    ->name('orders');
Route::get('/vieworders', [\App\Http\Controllers\UserController::class , 'viewOrders'])
    ->name('vieworders')
    ->middleware( 'auth');
Route::put('/orders/{id}',[\App\Http\Controllers\UserController::class, 'UpdateOder'])
    ->name('orders.update');

Route::get('order/{id}',[HomeController::class,'show'])
    ->name('order.show')
    ->middleware('permission:product_show', 'auth');

// -------------------Category ----------------------------\\
Route::prefix('categories')->name('categories.')->group(function() {
    Route::get('categories', [\App\Http\Controllers\CategoryController::class, 'index'])
        ->name('index')
        ->middleware('permission:category_view', 'auth');

    Route::get('/create', [\App\Http\Controllers\CategoryController::class, 'create'])
        ->name('create')
        ->middleware('permission:category_create', 'auth');

    Route::post('/store', [\App\Http\Controllers\CategoryController::class, 'store'])
        ->name('store')
        ->middleware('permission:category_store', 'auth');

    Route::get('/edit/{id}', [\App\Http\Controllers\CategoryController::class, 'edit'])
        ->name('edit')
        ->middleware('permission:category_edit', 'auth');

    Route::put('/update/{id}', [\App\Http\Controllers\CategoryController::class, 'update'])
        ->name('update')
        ->middleware('permission:category_update', 'auth');

    Route::delete('/delete/{id}', [\App\Http\Controllers\CategoryController::class, 'destroy'])
        ->name('destroy')
        ->middleware('permission:category_delete', 'auth');

    Route::get('/list', [\App\Http\Controllers\CategoryController::class, 'list'])
        ->name('list')
        ->middleware('permission:category_view', 'auth');
});

// -------------------Product ----------------------------\\
Route::get('products',[\App\Http\Controllers\ProductController::class,'index'])
    ->name('products.index')
    ->middleware('permission:product_view', 'auth');

Route::get('prodcuts/{id}',[\App\Http\Controllers\ProductController::class,'show'])
    ->name('products.show')
    ->middleware('permission:product_show', 'auth');

Route::get('products/create',[\App\Http\Controllers\ProductController::class,'create'])
    ->name('products.create')
    ->middleware('permission:product_create', 'auth');

Route::get('prodcuts/edit/{id}',[\App\Http\Controllers\ProductController::class,'edit'])
    ->name('products.edit')
    ->middleware('permission:product_edit', 'auth');

Route::post('products/store',[\App\Http\Controllers\ProductController::class,'store'])
    ->name('products.store')
    ->middleware('permission:product_store', 'auth');

Route::put('prodcuts/update/{id}',[\App\Http\Controllers\ProductController::class,'update'])
    ->name('products.update')
    ->middleware('permission:product_update', 'auth');

Route::delete('products/delete/{id}',[\App\Http\Controllers\ProductController::class,'destroy'])
    ->name('products.destroy')
    ->middleware('permission:product_delete', 'auth');

Route::get('products/list',[\App\Http\Controllers\ProductController::class,'list'])
    ->name('products.list')
    ->middleware('permission:product_view', 'auth');
//Orders
Route::get('/admin/orders',[\App\Http\Controllers\OrderController::class,'index'])
    ->name('admin.orders')->middleware('auth');


// -------------------Post ----------------------------\\
Route::get('posts',[\App\Http\Controllers\PostController::class,'index'])
    ->name('posts.index')
    ->middleware( 'auth');

Route::get('posts/{id}',[\App\Http\Controllers\PostController::class,'show'])
    ->name('posts.show')
    ->middleware( 'auth');

Route::get('post/create', [\App\Http\Controllers\PostController::class, 'create'])
    ->name('post.create')
    ->middleware('auth');

Route::get('posts/edit/{id}',[\App\Http\Controllers\PostController::class,'edit'])
    ->name('posts.edit')
    ->middleware( 'auth');

Route::post('posts/store',[\App\Http\Controllers\PostController::class,'store'])
    ->name('posts.store')
    ->middleware( 'auth');

Route::put('posts/update/{id}',[\App\Http\Controllers\PostController::class,'update'])
    ->name('posts.update')
    ->middleware('permission:product_update', 'auth');

Route::delete('posts/delete/{id}',[\App\Http\Controllers\PostController::class,'destroy'])
    ->name('posts.destroy')
    ->middleware( 'auth');

// -------------------VoteComment ----------------------------\\
Route::get('/admin/vote',[\App\Http\Controllers\VoteController::class,'index'])
    ->name('admin.votes.index')
    ->middleware('auth');

// -------------------USER ----------------------------\\
// -------------------Home ----------------------------\\
Route::get('/',[\App\Http\Controllers\User\HomeController::class,'index'])
    ->name('home.index');

// -------------------UserProduct ----------------------------\\
Route::get('/user/product/detail/{id}',[ProductController::class,'show'])
    ->name('user.product.show');
Route::get('/user/product/list',[ProductController::class,'list'])
    ->name('user.product.list');
Route::get('/user/product/listCategory/{id}',[ProductController::class,'listCategory'])
    ->name('user.product.listCategory');

// -------------------UserVoteProduct ----------------------------\\
Route::post('/user/product/detail/{id}',[\App\Http\Controllers\User\VoteController::class,'store'])
    ->name('user.vote.store')->middleware('auth');
Route::delete('/user/product/delete/{id}',[\App\Http\Controllers\User\VoteController::class,'delete'])
    ->name('user.vote.delete')->middleware('auth');

// -------------------Cart ----------------------------\\
Route::get('cart/',[\App\Http\Controllers\User\CartController::class,'index'])
    ->name('cart.index')->middleware('auth');
Route::post('cart/add-to-cart/',[\App\Http\Controllers\User\CartController::class,'addCart'])
    ->name('cart.addCart')->middleware('auth');
Route::put('cart/update/{id}',[\App\Http\Controllers\User\CartController::class,'updateCart'])
    ->name('cart.updateCart')->middleware('auth');
Route::delete('cart/delete/{id}',[\App\Http\Controllers\User\CartController::class,'deleteCart'])
    ->name('cart.deleteCart')->middleware('auth');
Route::post('cart/add-to-cart/{id}',[\App\Http\Controllers\User\CartController::class,'addCartDetail'])
    ->name('cart.addCart.detail')->middleware('auth');

// -------------------post ----------------------------\\
Route::get('/user/posts/',[PostController::class,'index'])
    ->name('user.posts.index');
Route::get('/user/posts/detail/{id}',[PostController::class,'show'])
    ->name('user.posts.detail');

// -------------------Contact Us ----------------------------\\
Route::get('/user/contact/',[HomeController::class,'contact'])
    ->name('user.contact');

// -------------------About ----------------------------\\
Route::get('/user/about/',[HomeController::class,'about'])
    ->name('user.about');

Route::get('redirect/{driver}', [\App\Http\Controllers\Auth\LoginController::class,'redirectToProvider'])
    ->name('login.provider')
    ->where('driver', implode('|', config('auth.socialite.drivers')));

Route::get('callBack',[\App\Http\Controllers\Auth\LoginController::class,'handleProviderCallback'])
    ->name('callback');

Route::get('user/products',[\App\Http\Controllers\ProductController::class,'userIndex'])
    ->name('products.user')
    ->middleware( 'auth');

Route::get('user/products/{id}',[\App\Http\Controllers\ProductController::class,'userShow'])
    ->name('products.detailuser')
    ->middleware( 'auth');
