<?php

namespace App\Services;

use App\Models\Vote;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class VoteService
{
    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create($request, $id)
    {
        $user = Auth::user();
        $dataCreate['product_id'] = $id;
        $dataCreate['stars'] = $request->stars;
        $dataCreate['comment'] = $request->comment;
        return $user->attachVote($dataCreate['product_id'],$dataCreate['stars'],$dataCreate['comment']);
    }


    public function delete($id)
    {
//        $user = Auth::user();
//        Vote::where('id', $id)->delete();

        return Vote::where('id', $id)->delete();
    }
}
