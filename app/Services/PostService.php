<?php

namespace App\Services;

use App\Traits\HandleImage;
use App\Repositories\PostRepository;

class PostService
{
    use HandleImage;
    protected $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function search($request)
    {
        $dataSearch = $request->all();
        $dataSearch['title'] = $request->name ?? '';

        return $this->postRepository->search($dataSearch);
    }

    public function create($request)
    {
        $dataCreate = $request->all();

        $dataCreate['image'] = $this->saveImage($request);

        return $this->postRepository->create($dataCreate);
    }

    public function find($id)
    {
        return $this->postRepository->find($id);
    }

    public function delete($id)
    {
        $category = $this->postRepository->find($id);

        $category->delete();

        $this->deleteImage($category->image);

        return $category;
    }

    public function update($request, $id)
    {
        $category = $this->postRepository->find($id);

        $dataUpdate = $request->all();

        $dataUpdate['image'] = $this->updateImage($request, $category->image);

        $category->update($dataUpdate);

        return $category;
    }

//    public function count()
//    {
//        return $this->categoryRepository->count();
//    }
}
