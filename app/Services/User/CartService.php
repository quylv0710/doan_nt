<?php

namespace App\Services\User;

use App\Repositories\ProductRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\Auth;

class CartService
{
    protected $userRepository;

    protected $productRepository;

    public function __construct(UserRepository $userRepository, ProductRepository $productRepository)
    {
        $this->userRepository = $userRepository;

        $this->productRepository = $productRepository;
    }

    public function carts($request, $product)
    {
        $user = Auth::user();
        $products = $product->find($request->product_id);
        $dataCreate['product_id'] =$request->product_id;
        $dataCreate['count'] = 1;
        if ($products->ticket > 0)
        {
            $priceSale = $products->price * $products->ticket / 100;
            $dataCreate['sum_price'] = $priceSale * $dataCreate['count'];
        } else
        {
            $dataCreate['sum_price'] = $products->price * $dataCreate['count'];
        }
        return $user->attachCart($dataCreate['product_id'],$dataCreate['count'],$dataCreate['sum_price']);
    }

    public function updateAddCart($request, $product)
    {
        $user = Auth::user();
        $products = $product->find($request->product_id);
        $dataCreate['count'] = $user->carts->find($request->product_id)->pivot->count +1 ;
        if ($products->ticket > 0)
        {
            $priceSale = $products->price * $products->ticket /100;
            $dataCreate['sum_price'] = $priceSale * $dataCreate['count'];
        } else
        {
            $dataCreate['sum_price'] = $products->price * $dataCreate['count'];
        }
        return $user->carts()->updateExistingPivot($products, array('count' => $dataCreate['count'], 'sum_price' => $dataCreate['sum_price']), false);
    }

    public function updateCart($request, $product,$id)
    {
        $user = Auth::user();
        $products = $product->find($id);
        $dataCreate['count'] = $request->quantity;
        if ($products->ticket > 0)
        {
            $priceSale = $products->price * $products->ticket /100;
            $dataCreate['sum_price'] = $priceSale * $dataCreate['count'];
        } else
        {
            $dataCreate['sum_price'] = $products->price * $dataCreate['count'];
        }

        return $user->carts()->updateExistingPivot($products, array('count' => $dataCreate['count'], 'sum_price' => $dataCreate['sum_price']), false);
    }

    public function delete($id)
    {
        $user = Auth::user();
        return $user->carts()->detach($id);
    }

    public function cartsDetail($request, $id)
    {
        $user = Auth::user();
        $products = $this->productRepository->find($id);
        $dataCreate['product_id'] = $id;
        $dataCreate['count'] = $request->quantity;
        if ($products->ticket > 0)
        {
            $priceSale = $products->price * $products->ticket /100;
            $dataCreate['sum_price'] = $priceSale * $dataCreate['count'];
        } else
        {
            $dataCreate['sum_price'] = $products->price * $dataCreate['count'];
        }
        return $user->attachCart($dataCreate['product_id'],$dataCreate['count'],$dataCreate['sum_price']);
    }

    public function updateAddCartDetail($request, $id)
    {
        $user = Auth::user();

        $products = $this->productRepository->find($id);

        $dataCreate['count'] = $user->carts->find($id)->pivot->count + $request->quantity ;
        if ($products->ticket > 0)
        {
            $priceSale = $products->price * $products->ticket /100;
            $dataCreate['sum_price'] = $priceSale * $dataCreate['count'];
        } else
        {
            $dataCreate['sum_price'] = $products->price * $dataCreate['count'];
        }
        return $user->carts()->updateExistingPivot($products, array('count' => $dataCreate['count'], 'sum_price' => $dataCreate['sum_price']), false);
    }
}
