<?php

namespace App\Services;

use App\Repositories\CategoryRepository;
use App\Repositories\OrderRepository;

class OderService
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function all()
    {
        return $this->orderRepository->all();
    }


    public function find($id)
    {
        return $this->orderRepository->find($id);
    }


    public function update($request)
    {
        $category = $this->orderRepository->find($request->id);
        $dataUpdate = $request->all();
        $category->update($dataUpdate);
        return $category;
    }
}
