<?php

namespace App\Repositories;

use App\Models\Product;

class ProductRepository extends BaseRepository
{

    public function model()
    {
        return Product::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withName($dataSearch['name'])
            ->withCategoryId($dataSearch['category_id'])
            ->withMinPrice($dataSearch['min'])
            ->withMaxPrice($dataSearch['max'])
            ->latest('id')->paginate(9);
    }

    public function searchCategory($dataSearch)
    {
        return $this->model->withCategoryId($dataSearch['category_id'])->latest('id')->paginate(9);
    }


    public function hotProduct()
    {
//        return DB::table('items')
//            ->join('products', 'items.product_id', '=', 'products.id')
//            ->where('shopping_cart_id', '=' , $id)
//            ->get();;
    }

    public function saleProduct()
    {
        return Product::where('ticket', '>', 0)->get()->take(4);
    }
}
