<?php

namespace App\Repositories;


use App\Models\Post;

class PostRepository extends BaseRepository
{

    public function model()
    {
        return Post::class;
    }

    public function search($dataSearch)
    {
        return $this->model->withTitle($dataSearch['title'])->latest('id')->paginate(5);
    }

    public function newPost()
    {
        return $this->model->latest()->get()->take(5);
    }

}
