<?php

namespace App\Repositories;

use App\Models\Category;
use App\Models\Orders;

class OrderRepository extends BaseRepository
{

    public function model()
    {
        return Orders::class;
    }
}
