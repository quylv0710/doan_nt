<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Http\Requests\VoteRequest;
use App\Models\Vote;
use App\Services\VoteService;
use Illuminate\Http\Request;

class VoteController extends Controller
{
    protected $votService;

    public function __construct(VoteService $votService)
    {
        $this->votService = $votService;

        $this->middleware('auth');
    }

    public function store(VoteRequest $request, $id)
    {
        $this->votService->create($request, $id);

        return back()->with(['message' => 'Đánh giá thành công']);
    }

    public function delete($id)
    {
        $this->votService->delete($id);

        return back()->with(['message' => 'Đã xóa thành công']);
    }
}
