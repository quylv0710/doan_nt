<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Repositories\PostRepository;
use App\Repositories\ProductRepository;
use App\Services\OderService;
use App\Services\PostService;
use App\Services\ProductService;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    protected $productService;

    protected $productRepository;

    protected $postRepository;

    protected $orderService;

    public function __construct(ProductService $productService, PostRepository $postRepository, ProductRepository $productRepository, OderService $orderService)
    {
        $this->productService = $productService;

        $this->postRepository = $postRepository;

        $this->productRepository = $productRepository;

        $this->orderService = $orderService;

    }

    public function index(Request $request){

        $products = $this->productService->search($request);

        $newProducts = $this->productService->newProduct();

        $newPosts = $this->postRepository->newPost();

//        $hotProduct = $this->productService->hotProduct ();

        $saleProducts = $this->productRepository->saleProduct ();

        return view('users.home.home', compact('saleProducts','newProducts', 'newPosts'));
    }

    public function contact()
    {
        return view('users.contact.contact');
    }

    public function about()
    {
        return view('users.about.about');
    }

    public function show($id)
    {
        $product = $this->orderService->find($id);
        $pr_id = $product->product_id;
        $products = $this->productService->find($pr_id);
        return view('admin.orders.show', compact('product','products'));
    }
}
