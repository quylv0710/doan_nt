<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Post;;

use App\Services\PostService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $productService;

    public function __construct(PostService $productService)
    {
        $this->productService = $productService;
    }

    public function index()
    {
        $posts = Post::paginate(5);

        return view('users.posts.list', compact('posts'));
    }

    public function show($id)
    {
        $post = $this->productService->find($id);

        $sames = Post::all()->random(5);

        return view('users.posts.detail', compact('post', 'sames'));
    }
}
