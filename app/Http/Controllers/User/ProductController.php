<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use App\Models\Vote;
use App\Repositories\CategoryRepository;
use App\Services\ProductService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{

    protected $productService;

    protected $categoryRepository;

    public function __construct(ProductService $productService, CategoryRepository $categoryRepository)
    {
        $this->productService = $productService;

        $this->categoryRepository = $categoryRepository;
    }

    public function show($id)
    {
        $product = $this->productService->find($id);

        $votes = Vote::where('product_id', $id)->get();

        $same = Product::all()->random(5);

//        $categories = $product->categories;

//        foreach ($categories as $category)
//        {
//            $category_id = $category->id;
//        }
//        $categorySame = Category::where('id', $category_id)->get();
//        $categorySame = $this->productService->searchCategory($category_id);

//        dd($product);

        return view('users.products.detail', compact('product', 'votes', 'same'));
    }

    public function list(Request $request)
    {
        $products = $this->productService->search($request);

        return view('users.products.list', compact('products'));
    }

    public function listCategory($id)
    {
        $products = $this->productService->searchCategory($id);

        $categoryName = $this->categoryRepository->find($id);

        return view('users.products.listCategory', compact('products','categoryName'));
    }
}
