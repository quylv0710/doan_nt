<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Vote;
use App\Repositories\User\CartRepository;
use App\Services\ProductService;
use App\Services\User\CartService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $cartService;
    protected $productService;

    public function __construct(CartService $cartService, ProductService $productService)
    {
        $this->cartService = $cartService;

        $this->productService = $productService;
    }

    public function index()
    {
        $user_id = Auth::user();

        $carts = $user_id->carts;

        $sumCart = Cart::where('user_id', $user_id->id)->sum('sum_price');

        return view('users.carts.cart', compact('carts', 'sumCart' ));
//        return view('User.cart', compact('carts'));
    }

    public function addCart(Request $request)
    {
        $product = $this->productService->all();

        $productArray = [];

        foreach(Auth::user()->carts as $cart)
        {
            array_push($productArray, $cart->id);
        }
        if(in_array( $request->product_id, $productArray))
        {
            $this->cartService->updateAddCart($request, $product);
        }
        else{
            $this->cartService->carts($request, $product);
        }
        return back()->with(['message' => 'thêm mới thành công']);
    }

    public function addCartDetail(Request $request, $id)
    {

        $productArray = [];

        foreach(Auth::user()->carts as $cart)
        {
            array_push($productArray, $cart->id);
        }
        if(in_array( $request->product_id, $productArray))
        {
            $this->cartService->updateAddCartDetail($request, $id);
        }
        else{
            $this->cartService->cartsDetail($request, $id);
        }
        return back()->with(['message' => 'thêm mới thành công']);
    }


    public function deleteCart($id)
    {
        $this->cartService->delete($id);

        return back()->with(['message' => 'đã xóa sản phẩm khỏi giỏ hàng']);
    }

    public function updateCart(Request $request, $id)
    {
        $product = $this->productService->all();
        if ($request->quantity < 1)
        {
            $this->cartService->delete($id);
        } else{
            $this->cartService->updateCart($request, $product, $id);
        }
        return back()->with(['message' => 'đã cập nhật sản phẩm thành công']);
    }
}
