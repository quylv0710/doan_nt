<?php

namespace App\Http\Controllers;

use App\Http\Requests\PostRequest;
use App\Services\PostService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    protected $postService;

    public function __construct(PostService $postService)
    {
        $this->postService = $postService;
    }

    public function index(Request $request)
    {
        $posts = $this->postService->search($request);

        return view('admin.posts.index', compact('posts'));
    }

    public function create()
    {
        return view('admin.posts.create');
    }

    public function store(PostRequest $request)
    {
        $this->postService->create($request);

        return redirect()->route('posts.index')->with('message', 'Post Create successfully');
    }

    public function show($id)
    {
        $post = $this->postService->find($id);
        return view('admin.posts.show', compact('post'));
    }

    public function edit($id)
    {
        $post = $this->postService->find($id);
        return view('admin.posts.edit', compact('post'));
    }

    public function update(PostRequest $request, $id)
    {
        $this->postService->update($request, $id);

        return redirect()->route('posts.index')->with('message', 'Post updated successfully');
    }

    public function destroy($id)
    {
        $this->postService->delete($id);

        return back()->with(['message' => 'Delete post success']);
    }
}
