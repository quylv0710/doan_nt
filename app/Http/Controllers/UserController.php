<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\UserRequest;
use App\Models\Cart;
use App\Models\Product;
use App\Repositories\PostRepository;
use App\Repositories\ProductRepository;
use App\Services\OderService;
use App\Services\ProductService;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//// Include library files
//require 'PHPMailer/Exception.php';
//require 'PHPMailer/PHPMailer.php';
//require 'PHPMailer/SMTP.php';

class UserController extends Controller
{
    protected $productService;

    protected $productRepository;

    protected $postRepository;

    protected $roleService;

    protected $userService;

    protected $orderService;

    public function __construct(RoleService $roleService, UserService $userService, ProductService $productService, PostRepository $postRepository, ProductRepository $productRepository, OderService $orderService)
    {
        $this->roleService = $roleService;
        $this->userService = $userService;
        view::share('roles', $this->roleService->all());
        $this->productService = $productService;

        $this->postRepository = $postRepository;

        $this->productRepository = $productRepository;
        $this->orderService = $orderService;
    }

    public function index(Request $request)
    {
        $users = $this->userService->search($request);

        return view('admin.users.index', compact('users'));
    }

    public function create()
    {
        return view('admin.users.create');
    }

    public function store(UserRequest $request)
    {
        $this->userService->create($request);
        return redirect(route('users.index'))->with('message', 'User created successfully ');
    }

    public function show($id)
    {
        $user = $this->userService->find($id);
        return view('admin.users.show', compact('user'));
    }

    public function edit($id)
    {
        $user = $this->userService->find($id);
        return view('admin.users.edit', compact('user'));
    }

    public function update(UpdateUserRequest $request, $id)
    {
        $this->userService->update($request, $id);

        return redirect()->route('users.index')->with('message', 'User updated successfully');
    }

    public function destroy($id)
    {
        $this->userService->destroy($id);
        return redirect(route('users.index'))->with('message', 'User delete successfully');
    }

    public function viewOrders( Request $request) {

        $products = $this->productService->search($request);

        $newProducts = $this->productService->newProduct();

        $newPosts = $this->postRepository->newPost();

//        $hotProduct = $this->productService->hotProduct ();

        $saleProducts = $this->productRepository->saleProduct ();

        return view('users.home.home', compact('saleProducts','newProducts', 'newPosts'));
//        return \view('');
    }

    public function orders(Request $request) {
        $requestOrder = $request->all();
        unset($requestOrder['_token']);
        $this->userService->orders($requestOrder);
        $this->sendMail();
        Auth::user()->carts()->detach($request->id);
        return redirect(route('cart.index'))->with('message', 'successfully');
    }

    public function UpdateOder(Request $request) {
        $this->orderService->Update($request);
        return back()->with(['message' => 'successfully']);
    }


    public function sendMail() {
        $user = Auth::user();
        $cart = Cart::where('user_id', $user->id)->first();
        $product= Product::find($cart->product_id);
        $SL = $cart->sum_price / $product->price *100 /$product->ticket;
        $mail = new PHPMailer(true);
        $mail->IsSMTP();
        $mail->Host = 'smtp.gmail.com';
        $mail->SMTPAuth = true;
        $mail->Username = 'quylv0710@gmail.com';
        $mail->Password = 'uuigspjxqsyermzv';
        $mail->SMTPSecure = 'ssl';
        $mail->Port = 465;
        $mail->setFrom('quylv0710@gmail.com', 'Hygge');
        $mail->addAddress($user->email);
        $mail->isHTML(true);
        $mail->Subject = "Sieu Thi Noi That Hy-GGE";
        $mail->Body  = "
            <html>
            <head>
            <title>Thông báo đặt hàng thành công</title>
            </head>
            <body>
            <p>Dear {$user->name}!</p>
            <p>Cảm ơn bạn đã tin tưởng và đặt đơn hàng của chúng tôi!</p>
            <p>Đơn hàng của bạn bao gồm:</p>
            <table>
            <tr>
            <th>Mã SP</th>
            <th>Tên</th>
            <th>SL</th>
            <th>Giá</th>
            <th>KM</th>
            <th>Tổng tiền</th>
            </tr>
            <tr>
            <td>{$cart->product_id}</td>
            <td>{$product->name}</td>
            <td>{$SL}</td>
            <td>{$product->price}đ</td>
            <td>{$product->ticket}%</td>
            <td>{$cart->sum_price}</td>
            </tr>
            </table>
            <p>Được gửi tới địa chỉ: {$user->address}</p>
            <p>Số điện thoại người nhận: {$user->phone_number}</p>
            <p>Đơn háng sẽ được gửi tới vào vài ngày tới, bui vòng để ý điện thoại nhận hàng</p>
            </body>
            </html>
        ";
        $mail->send();
    }
}
