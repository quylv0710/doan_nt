<?php

namespace App\Http\Controllers;

use App\Models\Orders;
use App\Models\User;
use App\Services\RoleService;
use App\Services\UserService;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    protected $roleService;

    protected $userService;

    public function __construct(RoleService $roleService, UserService $userService)
    {
        $this->roleService = $roleService;
        $this->userService = $userService;
    }


    public function index()
    {
        $users = Orders::paginate(10);
        return view('admin.orders.inex', compact('users'));
    }
}
