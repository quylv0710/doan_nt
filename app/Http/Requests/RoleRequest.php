<?php

namespace App\Http\Requests;

use App\Models\Role;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'display_name' => 'required | unique:roles',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Please enter roles name',
            'display_name.required' => 'Please enter roles display_name',
            'display_name.unique' => 'Roles is exits'
        ];
    }
}
