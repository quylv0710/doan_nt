<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use App\Rules\VnPhoneNumber;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;


class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => ['required', Rule::unique('users')->ignore($this->id)],
            'birthday' => 'required',
            'address' => 'required',
            'phone_number' => ['required', new VnPhoneNumber()],
            'gender' => 'required',
        ];
    }
}
