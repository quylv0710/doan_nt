<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = 'products';
    protected $fillable = [
        'name',
        'price',
        'description',
        'ticket',
        'image'
    ];

    public function categories()
    {
        return $this->belongsToMany(
            Category::class,
            'category_product',
            'product_id',
            'category_id'
        );
    }

    public function carts()
    {
        return $this->belongsToMany(
            User::class,
            'carts',
            'product_id',
            'user_id'
        )->withPivot('count', 'sum_price');
    }

    public function orders()
    {
        return $this->belongsToMany(
            User::class,
            'orders',
            'product_id',
            'user_id'
        )->withPivot('status', 'start', 'end', 'sum_price');
    }

    public function voteProducts()
    {
        return $this->belongsToMany(
            User::class,
            'user_vote_products',
            'product_id',
            'user_id'
        )->withPivot('stars', 'comment');
    }

    public function attachCategory($categoryId)
    {
        return $this->categories()->attach($categoryId);
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithMinPrice($query, $minPrice)
    {
        return $minPrice ? $query->where('price', '>', $minPrice) : null;
    }

    public function scopeWithMaxPrice($query, $maxPrice)
    {
        return $maxPrice ? $query->where('price', '<', $maxPrice) : null;
    }

    public function scopeWithCategoryId($query, $categoryId)
    {
        return $categoryId ? $query->WhereHas('categories', fn($category) => $category
            ->where('categories.id', $categoryId)) : null ;
    }

    public function syncCategory($categoryId)
    {
        return $this->categories()->sync($categoryId);
    }

    public function detachCategory()
    {
        return $this->categories()->detach();
    }

}
