<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vote extends Model
{
    use HasFactory;

    protected $table = 'user_vote_products';

    protected $fillable = [
        'user_id',
        'product_id',
        'stars',
        'comment'
    ];
}
