<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';
    protected $fillable=[
        'category_id',
        'title',
        'description',
        'content',
        'image'
    ];

    public function categories()
    {
        return $this->hasMany(Category::class, 'category_id', 'id');
    }

    public function scopeWithTitle($query, $name)
    {
        return $name ? $query->where('title', 'like', '%' . $name . '%') : null;
    }
}
