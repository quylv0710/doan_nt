<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'birthday',
        'address',
        'phone_number',
        'gender',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function roles()
    {
        return $this->belongsToMany(
            Role::class,
            'role_user',
            'user_id',
            'role_id'
        );
    }

    public function carts()
    {
        return $this->belongsToMany(
            Product::class,
            'carts',
            'user_id',
            'product_id'
        )->withPivot('count', 'sum_price');
    }

    public function orders()
    {
        return $this->belongsToMany(
            Product::class,
            'orders',
            'user_id',
            'product_id'
        )->withPivot('status', 'start', 'end', 'sum_price');
    }

    public function voteProducts()
    {
        return $this->belongsToMany(
            Product::class,
            'user_vote_products',
            'user_id',
            'product_id'
        )->withPivot('stars', 'comment');
    }

    public function scopeWithName($query, $name)
    {
        return $name ? $query->where('name', 'like', '%' . $name . '%') : null;
    }

    public function scopeWithEmail($query, $email)
    {
        return $email ? $query->where('email', $email) : null;
    }

    public function scopeWithRoleId($query, $roleId)
    {
        return $roleId ? $query->WhereHas('roles', fn($role) => $role->where('role_id', $roleId)) : null;
    }

    public function syncRole($roleId)
    {
        return $this->roles()->sync($roleId);
    }

    public function attachRole($roleId)
    {
        return $this->roles()->attach($roleId);
    }

    public function attachOrders($productId, $status, $start, $end, $address, $sumPrice) {
        return $this->orders()->attach($productId,array('status' => $status ,'start' => $start,'end' => $end, 'address' => $address ,'sum_price' => $sumPrice));
    }

    public function syncOrders($productId, $status, $start, $end, $sumPrice) {
        return $this->orders()->updateExistingPivot($productId,array('status' => $status ,'start' => $start,'end' => $end, 'sum_price' => $sumPrice));
    }

    public function hasPermission($permission)
    {
        foreach ($this->roles as $role) {
            if ($role->hasPermission($permission)) {
                return true;
            }
        }
        return false;
    }

    public function hasRole($role)
    {
        return $this->roles->contains('name', $role);
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('super_admin');
    }

    public function detachRole()
    {
        return $this->roles()->detach();
    }

    public function attachCart($productId, $count, $sum_price) {

        return $this->carts()->attach($productId,array('count' => $count ,'sum_price' => $sum_price));
    }

    public function syncCart($productId, $count, $sum_price) {

        return $this->carts()->syncWithPivotValues($productId,array('count' => $count ,'sum_price' => $sum_price));
    }

    public function attachVote($productId, $stars, $comment) {

        return $this->voteProducts()->attach($productId,array('stars' => $stars ,'comment' => $comment));
    }
}
