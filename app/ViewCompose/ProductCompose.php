<?php

namespace App\ViewCompose;

use App\Services\ProductService;
use Illuminate\View\View;

class ProductCompose
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function compose(View $view)
    {
        $view->with('productCount',$this->productService->count());
    }
}
