<?php

namespace App\ViewCompose;

use App\Services\CategoryService;
use App\Services\ProductService;
use Illuminate\Support\Facades\View;

class CategoryCompose
{
    protected $productService;

    protected $categoryservice;

    public function __construct(ProductService $productService, CategoryService $categoryService)
    {
        $this->productService = $productService;
        $this->categoryservice = $categoryService;
    }

    public function compose(\Illuminate\View\View $view)
    {
        $view->with('categories',$this->categoryservice->all());
        $view->with('categoryCount',$this->categoryservice->count());
    }
}
