<?php

namespace App\ViewCompose;

use App\Models\User;
use App\Services\UserService;

class CartCompose
{

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function compose(\Illuminate\View\View $view)
    {
        $view->with('countProductInCart', $this->countProductInCart());
    }

    public function countProductInCart()
    {
        if (auth()->check())
        {
            $countCart = auth()->user()->carts->count();
            return $countCart ? $countCart : 0;
        }
        return 0;
    }
}
