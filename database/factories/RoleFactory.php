<?php

namespace Database\Factories;

use Illuminate\Support\Str;
use App\Models\Role;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Role>
 */
class RoleFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $name = $this->faker->jobTitle();
        return [
            'name' => Str::slug($name, '-'),
            'display_name' => $name
        ];
    }
}
